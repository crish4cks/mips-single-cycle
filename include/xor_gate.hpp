#ifndef XOR_GATE_HPP
#define XOR_GATE_HPP

#include "systemc.h"

template <unsigned n>
SC_MODULE(xor_gate) {
    sc_in<sc_logic> A[n];
    sc_out<sc_logic> Y;
  
    void xor_generic() { 
        sc_logic tmp;
	tmp = A[0].read();
        for(unsigned i = 1; i < n; i++)
	    tmp = tmp ^ A[i].read();
        Y.write(tmp);
    }
  
    SC_CTOR(xor_gate) {
        SC_METHOD(xor_generic);
	for(unsigned i = 0; i < n; i++)
            sensitive << A[i];
    }
};

#endif

