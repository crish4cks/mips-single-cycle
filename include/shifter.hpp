#ifndef SHIFTER_HPP
#define SHIFTER_HPP

#include "systemc.h"

template <unsigned amount = 2>
SC_MODULE(shifter) {
    sc_in<sc_logic> INPUT[32], DIRECTION;
    sc_out<sc_logic> OUTPUT[32];

    void update_result() {
        sc_lv<32> tmp;
        for(unsigned i = 0; i < 32; i++)
            tmp[i] = INPUT[i].read();
        if(DIRECTION == '0')      // sll
            tmp = tmp << amount;
        else if(DIRECTION == '1') // srl
            tmp = tmp >> amount;
       for(unsigned i = 0; i < 32; i++)
           OUTPUT[i].write(tmp[i]);
    }

    SC_CTOR(shifter) {
        SC_METHOD(update_result);
        for(unsigned i = 0; i < 32; i++)
            sensitive << INPUT[i];
        sensitive << DIRECTION;
    }
};

#endif

