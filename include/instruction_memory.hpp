//
//   Instruction Memory
//
//   You can load a custom set of instructions by overwriting 
//   the content of the file called "rom.txt"
//

#ifndef INSTRUCTION_MEMORY_HPP
#define INSTRUCTION_MEMORY_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <cmath>
#include "systemc.h"

using namespace std;

const unsigned I_SIZE = 32;

template <unsigned addr_space = I_SIZE>
SC_MODULE(instruction_memory) {
    sc_in<sc_logic> ADDRESS[I_SIZE];
    sc_out<sc_logic> DATA[I_SIZE];
  
    void getInstruction() {
        sc_lv<I_SIZE> address, instruction;
        for(unsigned i = 0; i < I_SIZE; i++)
	    address[i] = ADDRESS[i].read();
	
        instruction = ( mem.at(address.to_uint()), 
                        mem.at(address.to_uint() + 1), 
                        mem.at(address.to_uint() + 2), 
                        mem.at(address.to_uint() + 3) );
	
	for(unsigned i = 0; i < I_SIZE; i++)
	    DATA[i].write(instruction[i]);          
    }
  
    SC_CTOR(instruction_memory) {
        assert(addr_space <= I_SIZE);
	dim = static_cast<unsigned>(pow(2, addr_space));
        mem.init(dim);
	this->init();
	this->load();
        SC_METHOD(getInstruction);
	for(unsigned i = 0; i < I_SIZE; i++)
	    sensitive << ADDRESS[i];
	dont_initialize();
    }  
  
    private:
        sc_vector<sc_lv<8> > mem;
	unsigned dim;
	
	void init() {
	    for(unsigned i = 0; i < dim; i++) {
	        mem.at(i) = sc_lv<8>("00000000");
		DATA[i].initialize(SC_LOGIC_0);
	    }	  
	}
	
        void load() {
	    string mem_byte;
	    unsigned i = 0;
	    ifstream mem_file("../test/data/rom.txt");
	    if(mem_file.is_open()) {
	        while(getline(mem_file, mem_byte)) {
		    mem.at(i) = sc_lv<8>(mem_byte.c_str());
		    i++;
                }
		mem_file.close();
	    }    
	    else cout << "Unable to open rom.txt in read mode!";
        }
};

#endif
