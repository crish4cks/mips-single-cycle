#ifndef REG_HPP
#define REG_HPP

#include "systemc.h"

template <unsigned n = 32>
SC_MODULE(reg) {
    sc_in<bool> CLK;
    sc_in<sc_logic> RST, LOAD, D[n];
    sc_out<sc_logic> Q[n];

    void update_result() {
        if(RST.read() == '0')
            for(unsigned i = 0; i < n; i++)
                Q[i].write(SC_LOGIC_0);
        else if(LOAD.read() == '1')
            for(unsigned i = 0; i < n; i++)
                Q[i].write( D[i].read() );
    }

    SC_CTOR(reg) {
        SC_METHOD(update_result);
        sensitive << CLK.pos() << RST;
    }
};

#endif

