#ifndef DATA_MEMORY_HPP
#define DATA_MEMORY_HPP

#include <cassert>
#include <cmath>
#include "systemc.h"

const unsigned D_SIZE = 32; 

template <unsigned addr_space = D_SIZE>
SC_MODULE(data_memory) {
    sc_in<bool> CLK;
    sc_in<sc_logic> RST, MEMREAD, MEMWRITE, ADDRESS[D_SIZE], WRITE_DATA[D_SIZE];
    sc_out<sc_logic> READ_DATA[D_SIZE];
    
    void reset() {
        if(RST.read() == '0')
            for(unsigned i = 0; i < dim; i++)
	        mem.at(i) = sc_lv<8>("00000000");
    }
    
    // Read the memory on falling edge
    void read_mem() {
        sc_lv<D_SIZE> address, read_data;
        for(unsigned i = 0; i < D_SIZE; i++)
	    address[i] = ADDRESS[i].read();
	
	if(MEMREAD.read() == '1') {
	    read_data = ( mem.at(address.to_uint()), 
		          mem.at(address.to_uint() + 1), 
		          mem.at(address.to_uint() + 2), 
		          mem.at(address.to_uint() + 3) );
    	
	    for(unsigned i = 0; i < D_SIZE; i++)
	        READ_DATA[i].write(read_data[i]);
	    
	} 
	else {
	    for(unsigned i = 0; i < D_SIZE; i++)
	        READ_DATA[i].write(SC_LOGIC_0);	          
	}
    }
    
    // Write the memory on rising edge
    void write_mem() {
        sc_lv<32> address, write_data;
	for(unsigned i = 0; i < D_SIZE; i++) {
	    address[i] = ADDRESS[i].read();
	    write_data[i] = WRITE_DATA[i].read();
	}
	
	if(MEMWRITE.read() == '1') {
	    mem.at( address.to_uint() ) = write_data.range(D_SIZE - 1, D_SIZE - 8);
	    mem.at( address.to_uint() + 1 ) = write_data.range(D_SIZE - 9, D_SIZE - 16);
	    mem.at( address.to_uint() + 2 ) = write_data.range(D_SIZE - 17, D_SIZE - 24);
	    mem.at( address.to_uint() + 3 ) = write_data.range(D_SIZE - 25, 0);
	}      
    }
    
    SC_CTOR(data_memory) {
        assert(addr_space <= D_SIZE);
	dim = static_cast<unsigned>(pow(2, addr_space));
        mem.init(dim);	
	SC_METHOD(reset);
	sensitive << RST;
	SC_METHOD(read_mem);
	sensitive << CLK.neg();
        SC_METHOD(write_mem);
	sensitive << CLK.pos();	
    }
    
    private:
        sc_vector<sc_lv<8> > mem;
	unsigned dim;
	
};

#endif
