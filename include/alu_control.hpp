#ifndef ALU_CONTROL_HPP
#define ALU_CONTROL_HPP

#include "systemc.h"

SC_MODULE(alu_control) {
    sc_in<sc_logic> ALUOP[2], FUNCTION_CODE[6];
    sc_out<sc_logic> OPERATION[4];
    
    void select_operation();
    
    SC_CTOR(alu_control) {
        SC_METHOD(select_operation);
	for(unsigned i = 0; i < 6; i++)
	    sensitive << FUNCTION_CODE[i];
	sensitive << ALUOP[0] << ALUOP[1];
    }
};

#endif