#ifndef REGISTER_FILE_HPP
#define REGISTER_FILE_HPP

#include "systemc.h"
#include "and_gate.hpp"
#include "decoder.hpp"
#include "mux.hpp"
#include "reg.hpp"

SC_MODULE(register_file) {
    sc_in<bool> CLK;
    sc_in<sc_logic> RST, REGWRITE, READ_REGISTER_1[5], READ_REGISTER_2[5], WRITE_REGISTER[5], WRITE_DATA[32];
    sc_out<sc_logic> READ_DATA_1[32], READ_DATA_2[32];
    
    // Declaration of the components    
    decoder<5, 32> dec0;
    mux<5, 32, 1024> mux0;
    mux<5, 32, 1024> mux1; 
    sc_vector<and_gate<2> > and_block;
    sc_vector<reg<32> > reg_bank;
    
    // Signals for connections between components
    sc_signal<sc_logic> D[32], W[32], S[1024];
    
    SC_CTOR(register_file) : dec0("Dec0"), mux0("Mux0"), mux1("Mux1"), and_block("AndBlock", 32), reg_bank("RegBank", 32) {
        unsigned mux_idx = 0; 
      
        for(unsigned i = 0; i < 5; i++) {
	    dec0.X[i](WRITE_REGISTER[i]);  
	    mux0.SEL[i](READ_REGISTER_1[i]);
	    mux1.SEL[i](READ_REGISTER_2[i]);	  
	}
                
        for(unsigned i = 0; i < 32; i++) {
	    dec0.Y[i](D[i]);  
	    mux0.Y[i](READ_DATA_1[i]);
	    mux1.Y[i](READ_DATA_2[i]);
	    and_block.at(i).A[0](REGWRITE);
	    and_block.at(i).A[1](D[i]);
	    and_block.at(i).Y(W[i]);
	    reg_bank.at(i).CLK(CLK);
	    reg_bank.at(i).RST(RST);
	    reg_bank.at(i).LOAD(W[i]);
	    
	    for(unsigned j = 0; j < 32; j++) {
	        reg_bank.at(i).D[j](WRITE_DATA[j]);  
		reg_bank.at(i).Q[j](S[mux_idx]);
		mux0.A[mux_idx](S[mux_idx]);
		mux1.A[mux_idx](S[mux_idx]);
		mux_idx++;
	    }
	}	
    }
};

#endif