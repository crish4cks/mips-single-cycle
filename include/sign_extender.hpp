#ifndef SIGN_EXTENDER_HPP
#define SIGN_EXTENDER_HPP

#include "systemc.h"
#include <cassert>

template <unsigned n = 16, unsigned m = 32>
SC_MODULE(sign_extender) {
    sc_in<sc_logic> INPUT[n];
    sc_out<sc_logic> OUTPUT[m];
  
    void update_result() {
            for(unsigned i = 0; i < n; i++)
                OUTPUT[i].write( INPUT[i].read() );
            for(unsigned i = n; i < m; i++)
                OUTPUT[i].write( INPUT[n - 1].read() );
    }
  
    SC_CTOR(sign_extender) {
        assert(n <= m);
        SC_METHOD(update_result);
        for(unsigned i = 0; i < n; i++)
            sensitive << INPUT[i];
    }
};

#endif

