#ifndef MIPS_HPP
#define MIPS_HPP

#include "systemc.h"
#include "datapath.hpp"
#include "control_unit.hpp"

SC_MODULE(mips) {
  
    sc_in<bool> CLK;
    sc_in<sc_logic> RST;
    sc_out<sc_logic> PROGRAM_COUNTER[32];
    sc_out<sc_logic> INSTRUCTION[32];
    sc_out<sc_logic> REGFILE_READ_REG_1[5]; 
    sc_out<sc_logic> REGFILE_READ_REG_2[5]; 
    sc_out<sc_logic> REGFILE_WRITE_REG[5];
    sc_out<sc_logic> REGFILE_WRITE_DATA[32];
    sc_out<sc_logic> REGFILE_READ_DATA_1[32];
    sc_out<sc_logic> REGFILE_READ_DATA_2[32];
    sc_out<sc_logic> MEM_DATA_ADDRESS[32];
    sc_out<sc_logic> MEM_READ_DATA[32];
    sc_out<sc_logic> MEM_WRITE_DATA[32];
    sc_out<sc_logic> ALUSRC;
    sc_out<sc_logic> MEMREAD; 
    sc_out<sc_logic> MEMWRITE;
    sc_out<sc_logic> MEMTOREG; 
    sc_out<sc_logic> REGDST; 
    sc_out<sc_logic> REGWRITE; 
    sc_out<sc_logic> BRANCH_SEL;
    sc_out<sc_logic> JUMP;
    sc_out<sc_logic> ALU_OPERATION[4];
    sc_out<sc_logic> Z, V;
    
    
    // Declaration of the components
    datapath dp0;
    control_unit ctrl0;
    
    
    // Signals for connections between components
    sc_signal<sc_logic> ALSRC0;                         // ALUSrc
    sc_signal<sc_logic> FN0[6];                         // Function Code
    sc_signal<sc_logic> MR0;                            // MemRead
    sc_signal<sc_logic> MW0;                            // MemWrite
    sc_signal<sc_logic> MTR0;                           // MemToReg
    sc_signal<sc_logic> RD0;                            // RegDst
    sc_signal<sc_logic> RW0;                            // RegWrite
    sc_signal<sc_logic> BSEL0;                          // BranchSel
    sc_signal<sc_logic> J0;                             // Jump
    sc_signal<sc_logic> OPR0[4];                        // ALU Operation
    sc_signal<sc_logic> OP0[6];                         // OpCode
    sc_signal<sc_logic> Z0;                             // Zero flag
    
    
    // Update debug signals
    void update_alusrc();
    void update_memread();
    void update_memwrite();
    void update_memtoreg();
    void update_regdst();
    void update_regwrite();
    void update_branchsel();
    void update_jump();
    void update_alu_operation();
    void update_z();
    
    
    SC_CTOR(mips) : dp0("DataPath"), ctrl0("ControlUnit") {
        
        // Control unit
        ctrl0.ALUSRC(ALSRC0);
	ctrl0.MEMREAD(MR0);
	ctrl0.MEMWRITE(MW0);
	ctrl0.MEMTOREG(MTR0);
	ctrl0.REGDST(RD0);
	ctrl0.REGWRITE(RW0);
	ctrl0.BRANCH_SEL(BSEL0);
	ctrl0.JUMP(J0);	
        for(unsigned i = 0; i < 6; i++) {
	    if(i < 4)
                ctrl0.OPERATION[i](OPR0[i]);  
	    ctrl0.OPCODE[i](OP0[i]);  
	    ctrl0.FUNCTION_CODE[i](FN0[i]);
	}
	ctrl0.Z(Z0);
	
	
	// Datapath
	dp0.CLK(CLK);
	dp0.RST(RST);
	dp0.ALUSRC(ALSRC0);
	dp0.MEMREAD(MR0);
	dp0.MEMWRITE(MW0);
	dp0.MEMTOREG(MTR0);
	dp0.REGDST(RD0);
	dp0.REGWRITE(RW0);
	dp0.BRANCH_SEL(BSEL0);
	dp0.JUMP(J0);
	dp0.Z(Z0);
	dp0.V(V);
	for(unsigned i = 0; i < 32; i++) {
	    if(i < 4)
	        dp0.OPERATION[i](OPR0[i]);  
	    if(i < 6) {
	        dp0.OPCODE[i](OP0[i]);
	        dp0.FUNCT[i](FN0[i]);
	    }
	    
	    // Update debug signals
            dp0.PROGRAM_COUNTER[i](PROGRAM_COUNTER[i]);
            dp0.INSTRUCTION[i](INSTRUCTION[i]);
	    if(i < 5) {
                dp0.REGFILE_READ_REG_1[i](REGFILE_READ_REG_1[i]); 
                dp0.REGFILE_READ_REG_2[i](REGFILE_READ_REG_2[i]); 
                dp0.REGFILE_WRITE_REG[i](REGFILE_WRITE_REG[i]);
	    }
            dp0.REGFILE_WRITE_DATA[i](REGFILE_WRITE_DATA[i]);
            dp0.REGFILE_READ_DATA_1[i](REGFILE_READ_DATA_1[i]);
            dp0.REGFILE_READ_DATA_2[i](REGFILE_READ_DATA_2[i]);
            dp0.MEM_DATA_ADDRESS[i](MEM_DATA_ADDRESS[i]);
            dp0.MEM_READ_DATA[i](MEM_READ_DATA[i]);
            dp0.MEM_WRITE_DATA[i](MEM_WRITE_DATA[i]);
	}
	
        SC_METHOD(update_alusrc);
	sensitive << ALSRC0;
	
        SC_METHOD(update_memread);
	sensitive << MR0;
	
        SC_METHOD(update_memwrite);
	sensitive << MW0;
	
        SC_METHOD(update_memtoreg);
	sensitive << MTR0;
	
        SC_METHOD(update_regdst);
	sensitive << RD0;
	
        SC_METHOD(update_regwrite);
	sensitive << RW0;
	
        SC_METHOD(update_branchsel);
	sensitive << BSEL0;
	
        SC_METHOD(update_jump);
	sensitive << J0;
	
	SC_METHOD(update_z);
	sensitive << Z0;
	
        SC_METHOD(update_alu_operation);
	for(unsigned i = 0; i < 4; i++)
	    sensitive << OPR0[i];
      
    }
};

#endif