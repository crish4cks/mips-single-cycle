//
//   Control
//
//   The supported instructions are the following ones:
//   - R-type
//   - Add immediate (addi)
//   - Load word (lw) 
//   - Store word (sw)
//   - Branch on equal (beq)
//   - Jump (j) 
//

#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "systemc.h"

SC_MODULE(control) {
    sc_in<sc_logic> OPCODE[6];
    sc_out<sc_logic> ALUOP[2], ALUSRC, MEMREAD, MEMWRITE, MEMTOREG, REGDST, REGWRITE, BRANCH, JUMP;
    
    void compute_control_signals();
    
    SC_CTOR(control) {
        SC_METHOD(compute_control_signals);
	for(unsigned i = 0; i < 6; i++)
	    sensitive << OPCODE[i];      
    }
  
};

#endif