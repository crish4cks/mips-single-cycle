#ifndef NOT_GATE_HPP
#define NOT_GATE_HPP

#include <systemc.h>

SC_MODULE(not_gate) {
    sc_in <sc_logic> A;
    sc_out <sc_logic> Y;
  
    void not_op() {
        Y.write(~A.read());
    }
  
    SC_CTOR(not_gate) {
        SC_METHOD(not_op);
        sensitive << A;
    }
};

#endif

