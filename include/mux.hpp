//
//   Generic Multiplexer
//
//   n_sel: the number of selection's bits
//   data_sz: the size in bits of the data to be selected
//   input_sz: the size of the input array (contains all the inputs)
//

#ifndef MUX_HPP
#define MUX_HPP

#include "systemc.h"
#include <string>
#include <cassert>
#include <cmath>

template <unsigned n_sel = 1, unsigned data_sz = 32, unsigned input_sz = 64>
SC_MODULE(mux) {
  
    sc_in<sc_logic> A[input_sz];
    sc_in<sc_logic> SEL[n_sel];
    sc_out<sc_logic> Y[data_sz];
    
    void select_data() {
        sc_lv<input_sz> a;
	sc_lv<n_sel> sel;
	sc_lv<data_sz> y;
      
        for(unsigned i = 0; i < n_sel; i++)
	    sel[i] = SEL[i].read();
        
	for(unsigned i = 0; i < input_sz; i++)
	    a[i] = A[i].read();
	
	// Select the correct portion of input bits and put them in output
	y = a.range(data_sz * (sel.to_uint() + 1) - 1, data_sz * sel.to_uint());
	
	for(unsigned i = 0; i < data_sz; i++)
	    Y[i].write(y[i]);
    }
  
    SC_CTOR(mux) {
        assert(data_sz * static_cast<unsigned>(pow(2, n_sel)) == input_sz);
	for(unsigned i = 0; i < data_sz; i++)
	    Y[i].initialize(SC_LOGIC_0);
        SC_METHOD(select_data);
	for(unsigned i = 0; i < input_sz; i++)
            sensitive << A[i];
	for(unsigned i = 0; i < n_sel; i++)
            sensitive << SEL[i];
    }
};

#endif