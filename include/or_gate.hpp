#ifndef OR_GATE_HPP
#define OR_GATE_HPP

#include "systemc.h"

template <unsigned n>
SC_MODULE(or_gate) {
    sc_in<sc_logic> A[n];
    sc_out<sc_logic> Y;
  
    void or_generic() {
        for(unsigned i = 0; i < n; i++)
            if(A[i].read() == '1') {
	        Y.write(SC_LOGIC_1);
		return;
	    }
	Y.write(SC_LOGIC_0);
    }
  
    SC_CTOR(or_gate) {
        SC_METHOD(or_generic);
	for(unsigned i = 0; i < n; i++)
            sensitive << A[i];
    }
};

#endif

