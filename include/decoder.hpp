#ifndef DECODER_HPP
#define DECODER_HPP

#include "systemc.h"
#include <cassert>
#include <cmath>

template <unsigned n = 5, unsigned m = 32>
SC_MODULE(decoder) {
    sc_in<sc_logic> X[n];
    sc_out<sc_logic> Y[m];
    
    void select_output() {
        unsigned tmp = bin2unsigned();
        for(unsigned i = 0; i < m; i++)	    
	    if(i == tmp)
	        Y[i].write(SC_LOGIC_1);
	    else Y[i].write(SC_LOGIC_0);      
    }
    
    SC_CTOR(decoder) {
        assert(m == static_cast<unsigned>(pow(2, n)));
	SC_METHOD(select_output);
	for(unsigned i = 0; i < n; i++)
	    sensitive << X[i];
    }
    
    private:
        
        unsigned bin2unsigned() const {
            unsigned res = 0;
	    sc_lv<1> tmp;
            for(unsigned i = 0; i < n; i++) {
	        tmp[0] = X[i].read();
	        res += tmp.to_uint() * static_cast<unsigned>(pow(2, i));  
	    }
	    return res;
        }
};

#endif