#ifndef FULL_ADDER_HPP
#define FULL_ADDER_HPP

#include "systemc.h"

SC_MODULE(full_adder) {
    sc_in<sc_logic> A, B, CIN;
    sc_out<sc_logic> Y, COUT;
  
    void sum();
  
    SC_CTOR(full_adder) {
        SC_METHOD(sum);
        sensitive << A << B << CIN;
    }
};

#endif
