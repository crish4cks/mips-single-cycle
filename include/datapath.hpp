#ifndef DATAPATH_HPP
#define DATAPATH_HPP

#include "systemc.h"
#include "alu32.hpp"
#include "data_memory.hpp"
#include "instruction_memory.hpp"
#include "register_file.hpp"
#include "sign_extender.hpp"
#include "shifter.hpp"


SC_MODULE(datapath) {
    sc_in<bool> CLK;
    sc_in<sc_logic> RST, ALUSRC, MEMREAD, MEMWRITE, MEMTOREG, REGDST, REGWRITE, BRANCH_SEL, JUMP, OPERATION[4];
    sc_out<sc_logic> OPCODE[6], FUNCT[6], Z, V;
    
    // Outputs used for debug
    sc_out<sc_logic> PROGRAM_COUNTER[32];
    sc_out<sc_logic> INSTRUCTION[32];
    sc_out<sc_logic> REGFILE_READ_REG_1[5]; 
    sc_out<sc_logic> REGFILE_READ_REG_2[5]; 
    sc_out<sc_logic> REGFILE_WRITE_REG[5];
    sc_out<sc_logic> REGFILE_WRITE_DATA[32];
    sc_out<sc_logic> REGFILE_READ_DATA_1[32];
    sc_out<sc_logic> REGFILE_READ_DATA_2[32];
    sc_out<sc_logic> MEM_DATA_ADDRESS[32];
    sc_out<sc_logic> MEM_READ_DATA[32];
    sc_out<sc_logic> MEM_WRITE_DATA[32];
    
    // Declaration of the components    
    alu32 add0, add1, alu0;                    // The two adders and the ALU
    data_memory<4> dat0;                       // Data memory
    instruction_memory<5> instr0;              // Instruction memory
    mux<1, 5, 10> mux0;                        // First multiplexer from the left (observing the datapath's scheme)
    mux<1, 32, 64> mux1, mux2, mux3, mux4;     // The other four multiplexers
    reg<32> pc0;                               // Program counter
    register_file regf0;                       // Register file
    sign_extender<16, 32> signext0;            // Sign extender
    shifter<2> shift0, shift1;                 // The two shifters
    
    
    // Signals for connections between components
    sc_signal<sc_logic> ONE, ZERO, FOUR[32];                    // Dummy signals used to handle constant inputs
    sc_signal<sc_logic> Z_OPEN[2], V_OPEN[2], COUT_OPEN[3];     // Dummy signals used to handle open wires
    
    
    sc_signal<sc_logic> A0[32];     // from first adder's output to second adder, third and fourth muxes inputs
    sc_signal<sc_logic> AL0[32];    // from ALU's output to data memory and last mux inputs
    sc_signal<sc_logic> A1[32];     // from second adder's output to third mux's input (from left)
    sc_signal<sc_logic> DT0[32];    // from data memory's output to last mux's input
    sc_signal<sc_logic> E0[32];     // from sign extender's output to second shifter and second mux inputs
    sc_signal<sc_logic> I0[32];     // from instruction memory's output to first shifter (from left), control unit, register file, sign extender and ALU control inputs
    sc_signal<sc_logic> M0[5];      // from first mux's output (from left) to register file input
    sc_signal<sc_logic> M1[32];     // from second mux's output (from left) to ALU operand ('B')
    sc_signal<sc_logic> M2[32];     // from third mux's output (from left) to fourth mux's input
    sc_signal<sc_logic> M3[32];     // from fourth mux's output (from left) to PC's input
    sc_signal<sc_logic> M4[32];     // from last mux's output (from left) to register file input
    sc_signal<sc_logic> P0[32];     // from PC's output to adder and instruction memory inputs
    sc_signal<sc_logic> R0[32];     // from register file's output to ALU operand ('A')
    sc_signal<sc_logic> R1[32];     // from register file's output to second mux and data memory inputs
    sc_signal<sc_logic> S0[32];     // from first shifter's output to fourth mux's input
    sc_signal<sc_logic> S1[32];     // from second shifter's output to second adder's input
    
    void update_opcode();
    void update_funct();
    
    // Update debug signals
    void update_program_counter();
    void update_instruction();
    void update_regfile_read_reg_1();
    void update_regfile_read_reg_2();
    void update_regfile_write_reg();
    void update_regfile_write_data();
    void update_regfile_read_data_1();
    void update_regfile_read_data_2();
    void update_mem_data_address();
    void update_mem_read_data();
    void update_mem_write_data();
    
    
    SC_CTOR(datapath) : add0("Adder0"), add1("Adder1"), alu0("ALU"), dat0("DataMemory"), instr0("InstructionMemory"), 
                        mux0("Mux0"), mux1("Mux1"), mux2("Mux2"), mux3("Mux3"), mux4("Mux4"), 
                        pc0("ProgramCounter"), regf0("RegisterFile"), signext0("SignExtender"),
                        shift0("Shifter0"), shift1("Shifter1") {
	
	// Initializes the signal to '1' (used for the LOAD input signal of the program counter)
	ONE.write(SC_LOGIC_1);
	
	// Initializes some input signals to '0'
	ZERO.write(SC_LOGIC_0);
	
	// Initializes the signal to four (used by the first adder to update the program counter)
	for(unsigned i = 0; i < 32; i++)
	    FOUR[i].write(SC_LOGIC_0);
	FOUR[2].write(SC_LOGIC_1);
	
	
	// Program counter
	pc0.CLK(CLK);
	pc0.RST(RST);
	pc0.LOAD(ONE);
        for(unsigned i = 0; i < 32; i++) {
	    pc0.D[i](M3[i]);
	    pc0.Q[i](P0[i]);	   
	}
	
	
	// The first adder
	add0.A_INV(ZERO);
	add0.B_NEG(ZERO);
	add0.OPERATION[1](ONE);
	add0.OPERATION[0](ZERO);
	for(unsigned i = 0; i < 32; i++) {
	    add0.A[i](P0[i]);
	    add0.B[i](FOUR[i]);
	    add0.RES[i](A0[i]);
	}
	add0.Z(Z_OPEN[0]);
	add0.V(V_OPEN[0]);
	add0.COUT(COUT_OPEN[0]);
	
	
	// The second adder
	add1.A_INV(ZERO);
	add1.B_NEG(ZERO);
	add1.OPERATION[1](ONE);
	add1.OPERATION[0](ZERO);
	for(unsigned i = 0; i < 32; i++) {
	    add1.A[i](A0[i]);
	    add1.B[i](S1[i]);
	    add1.RES[i](A1[i]);
	}
	add1.Z(Z_OPEN[1]);
	add1.V(V_OPEN[1]);
	add1.COUT(COUT_OPEN[1]);
		
	
	// Instruction memory
	for(unsigned i = 0; i < 32; i++) {
	    instr0.ADDRESS[i](P0[i]);
	    instr0.DATA[i](I0[i]);
	}
	
	
	// Sign extender
	for(unsigned i = 0; i < 32; i++) {
	    if(i < 16)
	        signext0.INPUT[i](I0[i]);  
	    signext0.OUTPUT[i](E0[i]);
	}
	
	
	// The first shifter
	shift0.DIRECTION(ZERO);
	for(unsigned i = 0; i < 32; i++) {
	    if(i < 26)
	        shift0.INPUT[i](I0[i]);  	    
	    else shift0.INPUT[i](ZERO);
	    shift0.OUTPUT[i](S0[i]);
	}
	
	
	// The second shifter
	shift1.DIRECTION(ZERO);
	for(unsigned i = 0; i < 32; i++) {
	    shift1.INPUT[i](E0[i]);  	    
	    shift1.OUTPUT[i](S1[i]);
	}
	
	
	// The first multiplexer
	mux0.SEL[0](REGDST);
	for(unsigned i = 0; i < 5; i++) {
            mux0.A[i](I0[i + 16]);
	    mux0.A[i + 5](I0[i + 11]);
	    mux0.Y[i](M0[i]);
        }
	
	
	// The second multiplexer
	mux1.SEL[0](ALUSRC);
	for(unsigned i = 0; i < 32; i++) {
            mux1.A[i](R1[i]);
	    mux1.A[i + 32](E0[i]);
	    mux1.Y[i](M1[i]);
        }
	
	
	// The third multiplexer
	mux2.SEL[0](BRANCH_SEL);
	for(unsigned i = 0; i < 32; i++) {
            mux2.A[i](A0[i]);
	    mux2.A[i + 32](A1[i]);
	    mux2.Y[i](M2[i]);
        }
	
	
	// The fourth multiplexer
	mux3.SEL[0](JUMP);
	for(unsigned i = 0; i < 32; i++) {
            mux3.A[i](M2[i]);
	    if(i < 28)
	        mux3.A[i + 32](S0[i]);
	    else mux3.A[i + 32](A0[i]);
	    mux3.Y[i](M3[i]);
        }
        
        
        // The last multiplexer
	mux4.SEL[0](MEMTOREG);
	for(unsigned i = 0; i < 32; i++) {
            mux4.A[i](AL0[i]);
	    mux4.A[i + 32](DT0[i]);
	    mux4.Y[i](M4[i]);
        }
	
	
	// Register file
	regf0.CLK(CLK);
	regf0.RST(RST);
	regf0.REGWRITE(REGWRITE);
	for(unsigned i = 0; i < 32; i++) {
	    if(i < 5) {
	        regf0.READ_REGISTER_1[i](I0[i + 21]);
	        regf0.READ_REGISTER_2[i](I0[i + 16]);
	        regf0.WRITE_REGISTER[i](M0[i]);
	    }
	    regf0.WRITE_DATA[i](M4[i]);
	    regf0.READ_DATA_1[i](R0[i]);
	    regf0.READ_DATA_2[i](R1[i]);
	}
	
	
	// ALU
	alu0.A_INV(OPERATION[3]);
	alu0.B_NEG(OPERATION[2]);
	alu0.OPERATION[1](OPERATION[1]);
	alu0.OPERATION[0](OPERATION[0]);
        for(unsigned i = 0; i < 32; i++) {
	    alu0.A[i](R0[i]);  
	    alu0.B[i](M1[i]);
	    alu0.RES[i](AL0[i]);
	}
	alu0.Z(Z);
	alu0.V(V);
	alu0.COUT(COUT_OPEN[2]);
	
	
	// Data memory
	dat0.CLK(CLK);
	dat0.RST(RST);
	dat0.MEMREAD(MEMREAD);
	dat0.MEMWRITE(MEMWRITE);
	for(unsigned i = 0; i < 32; i++) {
	    dat0.ADDRESS[i](AL0[i]);  
	    dat0.WRITE_DATA[i](R1[i]);
	    dat0.READ_DATA[i](DT0[i]);
	}
	
	
	
	SC_METHOD(update_opcode);
	for(unsigned i = 26; i < 32; i++)
	    sensitive << I0[i];	
	
	SC_METHOD(update_funct);
	for(unsigned i = 0; i < 6; i++)
	    sensitive << I0[i];
	
	SC_METHOD(update_program_counter);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << P0[i];
	
	SC_METHOD(update_instruction);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << I0[i];
	
	SC_METHOD(update_regfile_read_reg_1);
	for(unsigned i = 0; i < 5; i++)
	    sensitive << I0[i + 21];
	
	SC_METHOD(update_regfile_read_reg_2);
	for(unsigned i = 0; i < 5; i++)
	    sensitive << I0[i + 16];
	
        SC_METHOD(update_regfile_write_reg);
        for(unsigned i = 0; i < 5; i++)
            sensitive << M0[i];
	
	SC_METHOD(update_regfile_write_data);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << M4[i];
	
	SC_METHOD(update_regfile_read_data_1);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << R0[i];
	
	SC_METHOD(update_regfile_read_data_2);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << R1[i];
	
	SC_METHOD(update_mem_data_address);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << AL0[i];
	
	SC_METHOD(update_mem_read_data);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << DT0[i];
	
	SC_METHOD(update_mem_write_data);
	for(unsigned i = 0; i < 32; i++)
	    sensitive << R1[i];
	
    }
};

#endif