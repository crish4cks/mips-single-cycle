//
//   32 bit ALU
//
//   A_INV  B_NEG  OPERATION |  Action performed by the ALU
//   0      0      00        |        AND
//   0      0      01        |        OR
//   0      0      10        |        ADD
//   0      1      10        |        SUB
//   0      1      11        |        SLT (set on less than)
//   1      1      00        |        NOR
//

#ifndef ALU32_HPP
#define ALU32_HPP

#include "systemc.h"
#include "not_gate.hpp"
#include "or_gate.hpp"
#include "xor_gate.hpp"
#include "alu.hpp"


SC_MODULE(alu32) {
    sc_in<sc_logic> A[32], B[32], A_INV, B_NEG;
    sc_in<sc_logic> OPERATION[2];
    sc_out<sc_logic> RES[32], Z, V, COUT;
      
    // Declaration of the components
    not_gate not0;
    or_gate<32> or0;
    xor_gate<2> xor0;
    sc_vector<alu> alu_block;
    
    // Signals for connections between components (the SET array is used only to handle open wires)
    sc_signal<sc_logic> N0, C[32], N1[32], SET[31], ZERO;
    sc_signal<sc_logic> SET0;
    
    // Update the result
    void update_res();
    
    // Update the carry-out
    void update_cout();
    
    SC_CTOR(alu32) : not0("Not0"), or0("Or0"), xor0("Xor0"), alu_block("AluBlock", 32) {
         
        // Initializes the signal to zero (used for the ALU's LESS input)
        ZERO.write(SC_LOGIC_0);
      
        alu_block.at(0).A(A[0]);
	alu_block.at(0).B(B[0]);
	alu_block.at(0).CIN(B_NEG);
	alu_block.at(0).LESS(SET0);
	alu_block.at(0).A_INV(A_INV);
	alu_block.at(0).B_INV(B_NEG);
	alu_block.at(0).OPERATION[1](OPERATION[1]);
	alu_block.at(0).OPERATION[0](OPERATION[0]);
	alu_block.at(0).RES(N1[0]);
	alu_block.at(0).SET(SET[0]);
	alu_block.at(0).COUT(C[0]);
	
	or0.A[0](N1[0]);
	
	for(unsigned i = 1; i < 31; i++) {
	    alu_block.at(i).A(A[i]);
	    alu_block.at(i).B(B[i]);
	    alu_block.at(i).CIN(C[i - 1]);
	    alu_block.at(i).LESS(ZERO);
	    alu_block.at(i).A_INV(A_INV);
	    alu_block.at(i).B_INV(B_NEG);
	    alu_block.at(i).OPERATION[1](OPERATION[1]);
	    alu_block.at(i).OPERATION[0](OPERATION[0]);
	    alu_block.at(i).RES(N1[i]);
	    alu_block.at(i).SET(SET[i]);
	    alu_block.at(i).COUT(C[i]);
	    
	    or0.A[i](N1[i]);
	}
	
	alu_block.at(31).A(A[31]);
	alu_block.at(31).B(B[31]);
	alu_block.at(31).CIN(C[30]);
	alu_block.at(31).LESS(ZERO);
	alu_block.at(31).A_INV(A_INV);
	alu_block.at(31).B_INV(B_NEG);
	alu_block.at(31).OPERATION[1](OPERATION[1]);
	alu_block.at(31).OPERATION[0](OPERATION[0]);
	alu_block.at(31).RES(N1[31]);
	alu_block.at(31).SET(SET0);
	alu_block.at(31).COUT(C[31]);
	
	or0.A[31](N1[31]);
	
	// Zero flag
	or0.Y(N0);
	not0.A(N0);
	not0.Y(Z);
		
	// Overflow flag
	xor0.A[0](C[30]);
	xor0.A[1](C[31]);
	xor0.Y(V);
      
	
	SC_METHOD(update_res);
	for(unsigned i = 0; i < 32; i++)
            sensitive << N1[i];
	
	SC_METHOD(update_cout);
	sensitive << C[31];
	
    }
};

#endif
