//
//   Control Unit
//
//   It's composed by the 'Control' plus the 'ALU Control'
//

#ifndef CONTROL_UNIT_HPP
#define CONTROL_UNIT_HPP

#include "systemc.h"
#include "and_gate.hpp"
#include "alu_control.hpp"
#include "control.hpp"

SC_MODULE(control_unit) {
    sc_in<sc_logic> OPCODE[6], FUNCTION_CODE[6], Z;
    sc_out<sc_logic> ALUSRC, MEMREAD, MEMWRITE, MEMTOREG, REGDST, REGWRITE, BRANCH_SEL, JUMP, OPERATION[4];
    
    // Declaration of the components    
    and_gate<2> and0;
    alu_control ac0;
    control c0;    
    
    // Signals for connections between components
    sc_signal<sc_logic> X0, F0[2];
    
    SC_CTOR(control_unit) : and0("And0"), ac0("ALUControl0"), c0("Control0") {        
        and0.A[0](X0);
	and0.A[1](Z);
	and0.Y(BRANCH_SEL);
        
	for(unsigned i = 0; i < 6; i++) {
	    ac0.FUNCTION_CODE[i](FUNCTION_CODE[i]);
	    if(i < 2)
	        ac0.ALUOP[i](F0[i]);
	    if(i < 4)
	        ac0.OPERATION[i](OPERATION[i]);
	}
	
	c0.ALUSRC(ALUSRC);
	for(unsigned i = 0; i < 6; i++) {
	    if(i < 2)    
	        c0.ALUOP[i](F0[i]);	
	    c0.OPCODE[i](OPCODE[i]);
	}
	c0.MEMREAD(MEMREAD);
	c0.MEMWRITE(MEMWRITE);
	c0.MEMTOREG(MEMTOREG);
	c0.REGDST(REGDST);
	c0.REGWRITE(REGWRITE);
	c0.BRANCH(X0);
        c0.JUMP(JUMP);
    }
};

#endif