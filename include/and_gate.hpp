#ifndef AND_GATE_HPP
#define AND_GATE_HPP

#include "systemc.h"

template <unsigned n>
SC_MODULE(and_gate) {
    sc_in<sc_logic> A[n];
    sc_out<sc_logic> Y;
  
    void and_generic() {
        for(unsigned i = 0; i < n; i++)
            if(A[i].read() == '0') {
	        Y.write(SC_LOGIC_0);
		return;
	    }	  
        Y.write(SC_LOGIC_1);
    }
  
    SC_CTOR(and_gate) {
        SC_METHOD(and_generic);
	for(unsigned i = 0; i < n; i++)
            sensitive << A[i];
    }
};

#endif

