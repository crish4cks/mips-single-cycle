#ifndef ALU_HPP
#define ALU_HPP

#include "systemc.h"
#include "not_gate.hpp"
#include "and_gate.hpp"
#include "or_gate.hpp"
#include "full_adder.hpp"
#include "mux.hpp"

SC_MODULE(alu) {
    sc_in<sc_logic> A, B, CIN, LESS, A_INV, B_INV;
    sc_in<sc_logic> OPERATION[2];
    sc_out<sc_logic> RES, SET, COUT;
    
  
    // Declaration of the components
    not_gate not0, not1;
    mux<1, 1, 2> mux0, mux1;
    mux<2, 1, 4> mux2;
    and_gate<2> and0;
    or_gate<2> or0;
    full_adder fa0;
    
    // Signals for connections between components
    sc_signal<sc_logic> A0, M0, M1, O0, S0, X0, X1;
    
    // Update the SET output pin
    void update_set();
    
    SC_CTOR(alu) : not0("Not0"), not1("Not1"), mux0("Mux0"), mux1("Mux1"), mux2("Mux2"), and0("And0"), or0("Or0"), fa0("Fa0") {
      
        not0.A(A);
	not0.Y(M0);
	
	not1.A(B);
	not1.Y(M1);
	
	and0.A[0](X0);
	and0.A[1](X1);
	and0.Y(A0);
	
	or0.A[0](X0);
	or0.A[1](X1);
	or0.Y(O0);
	
	fa0.A(X0);
	fa0.B(X1);
	fa0.CIN(CIN);
	fa0.Y(S0);
	fa0.COUT(COUT);
	
	mux0.A[0](A);
	mux0.A[1](M0);
	mux0.SEL[0](A_INV);
	mux0.Y[0](X0);
	
	mux1.A[0](B);
	mux1.A[1](M1);
	mux1.SEL[0](B_INV);
	mux1.Y[0](X1);
        
	mux2.A[0](A0);
	mux2.A[1](O0);
	mux2.A[2](S0);
	mux2.A[3](LESS);
	mux2.SEL[0](OPERATION[0]);
	mux2.SEL[1](OPERATION[1]);
	mux2.Y[0](RES);
	
	SC_METHOD(update_set);
        sensitive << S0;
	
    }
};

#endif
