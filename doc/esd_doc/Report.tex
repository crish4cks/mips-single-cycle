\documentclass[11pt,a4paper,titlepage,twoside]{book}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
%\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}
%\usepackage{lscape}
\usepackage{adjustbox}
\usepackage{footnote}
\usepackage{pdflscape}
\usepackage[left=2.5cm,right=2.5cm,top=3.5cm,bottom=2.5cm,headsep=2cm,footskip=1.5cm]{geometry}


\usepackage{fancyhdr}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot[C]{\thepage}
\fancyhead[RO]{\nouppercase{\slshape\rightmark}}
\fancyhead[LE]{\nouppercase{\slshape\leftmark}}
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter\ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\setlength{\headheight}{15pt}


\providecommand{\abstract}{}
\usepackage{abstract}

\title{\textsc{A simple implementation of the\\MIPS Single-cycle architecture\\using the SystemC library}}
\author{Cristiano Urban}
\date{}

\begin{document}
\pagenumbering{roman}

\maketitle

\tableofcontents
\listoftables
\listoffigures
\cleardoublepage

\addcontentsline{toc}{chapter}{Abstract}
\markboth{Abstract}{Abstract}
\pagenumbering{arabic}
\begin{abstract}
\noindent
This report concerns the activity related to the Electronic Systems Design course (in italian, Progetto di Sistemi Elettronici).\\ 
The purpouse of the course is to provide the right tools in order to acquire familiarity with the software design of a digital system.\\
The work consists in the development of digital components by using the SystemC library\footnote{\url{http://www.accellera.org/downloads/standards/systemc}}. The development is split into several teams, each one having its own git repository. Each team corresponds to one person who works on one or more components and can cooperate with others.
\end{abstract}

\newpage
\chapter{Introduction}
\noindent
In this report I will briefly describe all the work done for the project related to the Electronic Systems Design course~\cite{ESD}.\\
In this specific case, my objective was to develope and simulate the MIPS Single-cycle architecture, which is a 32 bit architecture.\\
The source code of the entire project can be found here: \url{https://bitbucket.org/crish4cks/mips-single-cycle}.\\
In a first phase of the development, I created a specific branch in order to write down the code for a valid test bench model applicable to all the components. The idea was to incorporate both simulation and CMake tests into an unique entity. A small problem I encountered during this stage was due to the fact that the SystemC scheduler calls all the processes at zero time regardless of any event.\\  
Afterwards, I continued the work by creating and testing, one by one, all the components on a testing branch and merging the results when I was sure that all worked fine. In this way I kept clean the master branch.\\
During this central phase of the development, I collaborated with Francesco Tumaini by trying to solve some of the issues he opened on his own repository. He also helped me to solve some issues.\\
Towards the end, I opened a pair of issues, but these issues were mainly due to some errors into the connections between the components into the datapath and to the lack of a correct initialization for some components (in particular some multiplexers and the data memory).\\
The next chapter of this report will show a brief description of the main features of the architecture.\\
In the following chapters, instead, I will describe, also with the help of some UML diagrams, all the modules contained into the datapath and the control unit, showing also a simple top-level view of the design.\\
Finally, the last chapter explains how the whole architecture was tested, by reporting the code snippet of a simple program and showing the results of the simulation. 


\newpage
\chapter{Architecture overview}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.5]{./img/mips_single_cycle.png}
        \caption{The MIPS Single-cycle architecture}
        \label{figure:mips_arch}
\end{figure}
\section*{Main features}
\begin{itemize}
    \item Register-to-register (load \& store) architecture with 32 registers each one of 32 bits; also the addressing space is of 32 bits, thus the architecture is symmetric
    \item Each instruction executes in one clock cycle
    \item The duration of the clock cycle is chosen according to the longest (in terms of execution time) instruction; for us the longest one is the \emph{load word} beacuse it involves all the resources
    \item All the resources must be accessed only one time per clock cycle
    \item Two separated memory units for instructions and data (Harvard architecture)
    \item The datapath is asynchronous.
\end{itemize}
\section*{Format of the instructions}
The MIPS architecture provides three different types of format for the instructions:
\begin{itemize}
    \item R-type: arithmetic and logic instructions (i.e. add, sub, and, or, ...)
    \item I-type: memory access (i.e. lw, sw), conditional branches (i.e. beq, bne), operations with immediate values (i.e. addi, andi)
    \item J-type: mainly used for unconditional branches (i.e. j).
\end{itemize}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.35]{./img/instructions_format.png}
        \caption{The formats of the instructions}
        \label{figure:instr_format}
\end{figure}
The implementation described in this report supports the following instructions:
\begin{itemize}
    \item \textbf{R-type}
    \item \textbf{addi:} add immediate (I-type)
    \item \textbf{lw:} load word (I-type)
    \item \textbf{sw:} store word (I-type)
    \item \textbf{beq:} branch on equal (I-type)
    \item \textbf{j:} jump (J-type).
\end{itemize}
\newpage
\chapter{The Datapath} 
This chapter briefly describes all the components contained into the MIPS datapath.

\section{Logic gates}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/logic_gates.png}
        \caption{The logic gates}
        \label{figure:logic_gates_uml}
\end{figure}
\noindent
The first three classes on the top are template classes and provide a formal parameter which is used to specify the number of inputs of the gates. For this classes the interface is the same, except for the operation they perform.\\
The not gate, instead, is a standard class and has one input and one output, as expected.
\section{Full-adder}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/full_adder.png}
        \caption{The full\_adder class}
        \label{figure:full_adder_uml}
\end{figure}
\noindent
The full-adder computes the sum of two bits (A and B) plus a carry-in (CIN) and puts the result in Y providing also the carry-out (COUT).\\
The implementation of the class has been done in a behavioural way.
\newpage
\section{Multiplexer}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/mux.png}
        \caption{The mux class}
        \label{figure:mux_uml}
\end{figure}
\noindent
The mux class is a template class for a generic (in terms of number of bits) multiplexer, characterized by three formal parameters:
\begin{description}
    \item [n\_sel:] is the number of selection bits
    \item [data\_sz:] is the size in bits of the data to be selected
    \item [input\_sz:] is the size in bits of the input array which contains all the inputs in one unique block.
\end{description}
Let's suppose we want a multiplexer having four inputs each one of 32 bits, we can instantiate it by writing the following code:
\begin{verbatim}
    mux<2,32,128> mux0("MyMux");
\end{verbatim}
The code fragment here below shows how the multiplexer operates. According to the value of the selection bits, it retrieves the right portion of the input array and puts it on the output.
\begin{verbatim}
void select_data() {
    sc_lv<input_sz> a;
    sc_lv<n_sel> sel;
    sc_lv<data_sz> y;
      
    for(unsigned i = 0; i < n_sel; i++)
        sel[i] = SEL[i].read();
        
    for(unsigned i = 0; i < input_sz; i++)
        a[i] = A[i].read();
	
    // Select the correct portion of input bits and put them in output
    y = a.range(data_sz * (sel.to_uint() + 1) - 1, data_sz * sel.to_uint());
	
    for(unsigned i = 0; i < data_sz; i++)
        Y[i].write(y[i]);
}
\end{verbatim}

\newpage
\section{Arithmetic Logic Unit (ALU)}
\subsection{1 bit ALU}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/alu.png}
        \caption{The alu class}
        \label{figure:alu_uml}
\end{figure}
\noindent
The alu class has essentialy two operands (A and B), four control inputs (A\_INV, B\_INV, CIN and OPERATION[2]) and two outputs, one for the operation result and the other for the carry-out. The role of the LESS input and the SET output will be clarified later.\\
The operation supported by the ALU are the following ones:
\begin{description}
    \item [A and B:] can be performed by setting all the control inputs to zero
    \item [A or B:] can be performed by setting all the control inputs to zero, except the least significant bit of OPERATION which must be one (OPERATION = 01)
    \item [A + B:] the sum is obtained by setting OPERATION = 10 and the other control inputs to zero
    \item [A - B:] the subtraction is obtained by setting A\_INV = 0, B\_INV = 1, CIN = 1 and OPERATION = 10
    \item [A nor B:] can be performed by setting A\_INV = 1, B\_INV = 1 and the others to zero.
\end{description}
The figure here below shows the UML diagram for the structural implementation of the 1 bit ALU.
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.17]{./img/alu_structural.png}
        \caption{UML diagram for the 1 bit ALU}
        \label{figure:alu_structural}
\end{figure}
\newpage
\noindent
At the beginning, when I was writing the code for the alu module, I encountered my first "big" problem for which I also opened an issue on Bitbucket. This was due to the fact that I was trying to connect two single-bit ports of sc\_logic type with one of sc\_lv type. This is not allowed in SystemC, so I had to use an array of sc\_logic type in place of the one of sc\_lv type.

\subsection{32 bit ALU}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/alu32.png}
        \caption{The alu32 class}
        \label{figure:alu32_uml}
\end{figure}
\noindent
It is possible to build a 32 bit ALU by using thirty-two 1 bit ALUs and a few other adds, as shown into the figure here below.
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.25]{./img/alu32_structural.png}
        \caption{UML diagram for the 32 bit ALU}
        \label{figure:alu32_structural}
\end{figure} 
We can notice that, since CIN and B\_INV assume always the same values (for example 0 for the sum and 1 for the subtraction), we can use a single control line instead of two (called B\_NEG), thus the number of control bits for the 32 bit ALU becomes four (one bit for A\_INV and B\_NEG, two bits for OPERATION).\\
Another thing we notice is the presence of a connection between the SET output port of the most significant 1 bit ALU and the LESS input port of the least significant one. This add allows to perform a further operation which is called "set on less than" and sets the result of the ALU to 1 if A is less then B\footnote{A\textless B can also be seen as A-B\textless 0}, otherwise it sets the result to 0.\\
Finally, there are also a two inputs xor gate and a thirty-two inputs nor gate (an or gate plus a not gate). The first one checks if the carry-in and the carry-out values of the most significant 1 bit ALU are different (overflow condition), while the other one checks if the result of the ALU is zero (zero flag).\\

\begin{savenotes}
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|}
    \hline 
    \textbf{A\_INV} & \textbf{B\_NEG} & \textbf{OPERATION} & \textbf{Action performed} \\ 
    \hline 
    0 & 0 & 00 & and \\ 
    \hline 
    0 & 0 & 01 & or \\ 
    \hline 
    0 & 0 & 10 & add \\ 
    \hline 
    0 & 1 & 10 & sub \\ 
    \hline 
    0 & 1 & 11 & slt\footnote{Set on less than} \\ 
    \hline 
    1 & 1 & 00 & nor\footnote{For the second De Morgan's law, $\neg$(A $\lor$ B) = ($\neg$A) $\land$ ($\neg$B) } \\ 
    \hline 
\end{tabular}
\caption{The values of the control inputs for the ALU with the respective operations}
\end{table}
\end{savenotes}

\section{Shifter}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/shifter.png}
        \caption{The shifter class}
        \label{figure:shifter_uml}
\end{figure}
\noindent
The shifter class is a template class which implements a behavioural shifter. The amount of the shift can be fixed through the respective formal parameter. The direction of the shift is determined by the value of the DIRECTION input port (shift on left if DIRECTION = 0, shift on right if DIRECTION = 1).

\section{Sign Extender}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/sign_extender.png}
        \caption{The sign\_extender class}
        \label{figure:sign_extender_uml}
\end{figure}
\noindent
As the name suggests, this template class allows to extend the sign. Given a \textbf{n} bits word in input, it provides a \textbf{m} bits word (with m\textgreater n) by filling the remaining most significant m - n bits with zeros if the input number is positive, with ones otherwise.\\
As the shifter, also this component has been implemented in a behavioural way.

\newpage
\section{Decoder}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/decoder.png}
        \caption{The decoder class}
        \label{figure:decoder_uml}
\end{figure}
\noindent
The decoder class implements a behavioural n-to-m decoder, where \textbf{n} is the number of bits of the input and \textbf{m} is the number of bits of the output. The two formal parameters n and m are bound from the following relation: m = $2^n$.\\
For example, let's suppose we want a 5-to-32 decoder. We can instantiate it by writing the following code:
\begin{verbatim}
decoder<5,32> dec0("MyDecoder");
\end{verbatim}

\section{Register}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/reg.png}
        \caption{The reg class}
        \label{figure:register_uml}
\end{figure}
\noindent
The reg template class implements a generic register. The behaviour of this component is simple: when LOAD = 1 and a rising-edge event of the clock occurs, it overwrites the output with the value present in input. The register is also equipped with an asynchronous reset input (RST) which is an active-low signal.

\newpage
\section{Register file}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/register_file.png}
        \caption{The register\_file class}
        \label{figure:regfile_uml}
\end{figure}
\noindent
Since the MIPS architecture is a register-to-register architecture, almost all the operations involve the registers. Like the ALU, also the register file has been done in a structural way.\\
Here below is described the structure of the component, split in two parts: the read part and the write part.
\subsection{The read portion}
The read operation allows to retrieve the data from two registers at time. READ\_REG\_1 and READ\_REG\_2 inputs are used by the multiplexers to select the right outputs of the registers.
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.25]{./img/regfile_read_structural.png}
        \caption{UML diagram for the read portion of the register file}
        \label{figure:regfile_read_structural}
\end{figure}
\subsection{The write portion}
The write operation allows to store data into one register. The WRITE\_REG input provides the number of the register to be written. When a rising edge of the clock occurs and the REGWRITE input is set to 1, the value provided by the WRITE\_INPUT is stored into the right register.
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.23]{./img/regfile_write_structural.png}
        \caption{UML diagram for the write portion of the register file}
        \label{figure:regfile_write_structural}
\end{figure}

\newpage
\section{Memory units}
Instructions and data must be kept separated, thus we need two memory units. Both the memory units have been implemented in a behavioural way. 
\subsection{Instruction memory}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/instruction_memory.png}
        \caption{The instruction\_memory class}
        \label{figure:instruction_memory_uml}
\end{figure}
\noindent
The instruction\_memory template class implements a ROM\footnote{Read Only Memory}. It provides on the output the instruction corresponding to the address given in input.\\
The instructions are stored into a vector which is characterized by a number of elements\footnote{We address bytes, therefore, a single word is split into four bytes because each element of the vector stores exactly one byte} equal to $2^{addr\_space}$.\\
The class exploits two private methods in order to properly initialize the vector and load the instructions in binary format which are located into a text file called \emph{rom.txt} (the relative path of the file is \url{~/mips-single-cycle/test/data/rom.txt}).

\newpage
\subsection{Data memory}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/data_memory.png}
        \caption{The data\_memory class}
        \label{figure:data_memory_uml}
\end{figure}
\noindent
The data memory is usually used to read or store partial results of the operations performed on registers during the esecution of a well defined set of instructions.\\
Also in this case the data is stored into a vector whose size is determined in the same way of the instruction memory.\\
The read operation is performed on a falling edge of the clock, when the value of the MEMREAD input is set to 1.\\
The write operation, instead, is executed on a rising edge of the clock, when the value of the MEMWRITE input is set to 1. The data to be stored is given by the WRITE\_DATA input port.\\
The class provides also an active-low reset input (RST) used to clear the content of the memory array.

\chapter{The Control Unit}
This chapter contains a very brief description of the control's behaviour. The Control Unit is essentialy composed by two interconnected parts: the Control and the ALU Control.
\section{Control}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/control.png}
        \caption{The control class}
        \label{figure:control_uml}
\end{figure}
\noindent
The control class is responsible to set the right values of the control signals according to the six most significant bits of the instruction (the opcode).\\
At the code level, the implementation of the control consists of an sc\_method which computes the right values of the control signals (through a set of \emph{if} statements) every time the OPCODE input value changes.\\
The table here below summarizes the behaviour of the Control part.

%\begin{landscape}
\begin{table}[h]
\centering
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
    \hline 
       & \textbf{REGDST} & \textbf{REGWRITE} & \textbf{ALUSRC} & \textbf{ALUOP} & \textbf{MEMTOREG} & \textbf{MEMREAD} & \textbf{MEMWRITE} & \textbf{BRANCH} & \textbf{JUMP}\\ 
    \hline 
    \textbf{R-type} & 1 & 1 & 0 & 10 & 0 & 0 & 0 & 0 & 0\\ 
    \hline 
    \textbf{addi} & 0 & 1 & 1 & 00 & 0 & 0 & 0 & 0 & 0\\
    \hline 
    \textbf{lw} & 0 & 1 & 1 & 00 & 1 & 1 & 0 & 0 & 0\\
    \hline 
    \textbf{sw} & 0 & 0 & 1 & 00 & 0 & 0 & 1 & 0 & 0\\
    \hline 
    \textbf{beq} & 0 & 0 & 0 & 01 & 0 & 0 & 0 & 1 & 0\\
    \hline 
    \textbf{j} & 0 & 0 & 0 & 01 & 0 & 0 & 0 & 0 & 1\\
    \hline 
\end{tabular}
\end{adjustbox}
\caption{The supported instructions and the respective values for the control signals}
\end{table}
%\end{landscape}


\newpage
\section{ALU Control}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/alu_control.png}
        \caption{The alu\_control class}
        \label{figure:alu_control_uml}
\end{figure}
\noindent
The alu\_control class is used to set the right values for the four control lines of the ALU. The operation of the ALU is decided according to the values of the ALUOP and FUNCTION\_CODE inputs~\cite{Hennessy-Patterson}. 

\newpage
\chapter{Top-level view}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.3]{./img/mips.png}
        \caption{The mips class}
        \label{figure:mips_uml}
\end{figure}
\begin{figure}[h]
    \centering
        \includegraphics[scale=0.35]{./img/top_level.png}
        \caption{The top-level scheme}
        \label{figure:top_level_uml}
\end{figure}

\newpage
\chapter{Tests}
During the development phase each component has been well tested. I've provided a test bench file for almost all the components, with also the tests for CMake\footnote{\url{https://cmake.org/}}.\\
The simulation part of each test bench file includes a "stimulus object" which provides some stimuli to send to the DUT (in SystemC we call it Module Under Test) and a "checker object" which reads the results from the output ports.

\section*{A test for the whole architecture}
I've written a test in order to verify the correctness of the design. The test is simple but it has been chosen so that to use at least one instruction per format type.\\
The program increases ciclically the value of the t0 register of a quantity equal to 5 until it reaches the value of 20, then it exits. The assembly code of the program can be seen here below:
\begin{verbatim}
        addi $s2, $s2, 5     // $s2 = $s2 + 5 = 5
        addi $t2, $t2, 20    // $t2 = $t2 + 20 = 20
L1:     lw $t0, 12($t1)      // $t0 = MEM[3]
        add $t0, $s2, $t0    // $t0 = $s2 + $t0
        beq $t0, $t2, EXIT   // if ($t0 == $t2) goto EXIT
        sw $t0, 12($t1)      // MEM[3] = $t0
        j L1                 // jump to L1
EXIT:   addi $v0, $v0, 10    // exit condition
\end{verbatim}
Let's see how the instructions are placed into the ROM (refer also to the rom.txt file):
\begin{verbatim}
        [Instructions]         [ROM addresses]

        addi $s2, $s2, 5       00000000h
        addi $t2, $t2, 20      00000004h
 L1:    lw $t0, 12($t1)        00000008h
        add $t0, $s2, $t0      0000000Ch
        beq $t2, $t0, EXIT     00000010h
        sw $t0, 12($t1)        00000014h
        j L1                   00000018h
EXIT:   addi $v0, $v0, 10      0000001Ch
\end{verbatim}
The fragment here below, instead, shows the instructions written in assembly language (on the left) and their binary notation (on the right). The spaces between the bits in the binary representation mark the fields of the specific format for the instruction:
\begin{verbatim}
        [Assembly format]      [Binary format]

        addi $s2, $s2, 5       001000 10010 10010 0000000000000101
        addi $t2, $t2, 20      001000 01010 01010 0000000000010100
 L1:    lw $t0, 12($t1)        100011 01001 01000 0000000000001100
        add $t0, $s2, $t0      000000 10010 01000 01000 00000 100000
        beq $t2, $t0, EXIT     000100 01010 01000 0000000000000010
        sw $t0, 12($t1)        101011 01001 01000 0000000000001100
        j L1                   000010 00000000000000000000000010
EXIT:   addi $v0, $v0, 10      001000 00010 00010 0000000000001010
\end{verbatim}
The next page shows the result of the simulation obtained from the \emph{sim.log} file after having launched the MIPS test bench with the following command:
\begin{verbatim}
./tb_mips
\end{verbatim}


\newpage
\newgeometry{bottom=0.3cm,top=0.3cm,left=3cm,right=3cm}
\begin{landscape}
\begingroup
\fontsize{5pt}{11pt}\selectfont
\begin{verbatim}
   Time  CLK  RST  INSTRUCTION  PROGRAM_COUNTER  REGFILE_READ_REG_1  REGFILE_READ_REG_2  REGFILE_WRITE_REG  REGFILE_WRITE_DATA  REGFILE_READ_DATA_1  REGFILE_READ_DATA_2  MEM_DATA_ADDRESS  MEM_READ_DATA  MEM_WRITE_DATA  ALUSRC  MEMREAD  MEMWRITE  MEMTOREG  REGDST  REGWRITE  BRANCH_SEL  JUMP  ALU_OPERATION  Z  V
 100 ns  1    1    22520005     00000000         12                  12                  12                 00000005            00000000             00000000             00000005          00000000       00000000        1       0        0         0         0       1         0           0     2              0  0
 200 ns  1    1    214a0014     00000004         0a                  0a                  0a                 00000014            00000000             00000000             00000014          00000000       00000000        1       0        0         0         0       1         0           0     2              0  0
 300 ns  1    1    8d28000c     00000008         09                  08                  08                 00000000            00000000             00000000             0000000c          00000000       00000000        1       1        0         1         0       1         0           0     2              0  0
 400 ns  1    1    02484020     0000000c         12                  08                  08                 00000005            00000005             00000000             00000005          00000000       00000000        0       0        0         0         1       1         0           0     2              0  0
 500 ns  1    1    11480002     00000010         0a                  08                  08                 0000000f            00000014             00000005             0000000f          00000000       00000005        0       0        0         0         0       0         0           0     6              0  0
 600 ns  1    1    ad28000c     00000014         09                  08                  08                 0000000c            00000000             00000005             0000000c          00000000       00000005        1       0        1         0         0       0         0           0     2              0  0
 700 ns  1    1    08000002     00000018         00                  00                  00                 00000000            00000000             00000000             00000000          00000000       00000000        0       0        0         0         0       0         0           1     6              1  0
 800 ns  1    1    8d28000c     00000008         09                  08                  08                 00000005            00000000             00000005             0000000c          00000005       00000005        1       1        0         1         0       1         0           0     2              0  0
 900 ns  1    1    02484020     0000000c         12                  08                  08                 0000000a            00000005             00000005             0000000a          00000000       00000005        0       0        0         0         1       1         0           0     2              0  0
   1 us  1    1    11480002     00000010         0a                  08                  08                 0000000a            00000014             0000000a             0000000a          00000000       0000000a        0       0        0         0         0       0         0           0     6              0  0
1100 ns  1    1    ad28000c     00000014         09                  08                  08                 0000000c            00000000             0000000a             0000000c          00000000       0000000a        1       0        1         0         0       0         0           0     2              0  0
1200 ns  1    1    08000002     00000018         00                  00                  00                 00000000            00000000             00000000             00000000          00000000       00000000        0       0        0         0         0       0         0           1     6              1  0
1300 ns  1    1    8d28000c     00000008         09                  08                  08                 0000000a            00000000             0000000a             0000000c          0000000a       0000000a        1       1        0         1         0       1         0           0     2              0  0
1400 ns  1    1    02484020     0000000c         12                  08                  08                 0000000f            00000005             0000000a             0000000f          00000000       0000000a        0       0        0         0         1       1         0           0     2              0  0
1500 ns  1    1    11480002     00000010         0a                  08                  08                 00000005            00000014             0000000f             00000005          00000000       0000000f        0       0        0         0         0       0         0           0     6              0  0
1600 ns  1    1    ad28000c     00000014         09                  08                  08                 0000000c            00000000             0000000f             0000000c          00000000       0000000f        1       0        1         0         0       0         0           0     2              0  0
1700 ns  1    1    08000002     00000018         00                  00                  00                 00000000            00000000             00000000             00000000          00000000       00000000        0       0        0         0         0       0         0           1     6              1  0
1800 ns  1    1    8d28000c     00000008         09                  08                  08                 0000000f            00000000             0000000f             0000000c          0000000f       0000000f        1       1        0         1         0       1         0           0     2              0  0
1900 ns  1    1    02484020     0000000c         12                  08                  08                 00000014            00000005             0000000f             00000014          00000000       0000000f        0       0        0         0         1       1         0           0     2              0  0
   2 us  1    1    11480002     00000010         0a                  08                  08                 00000000            00000014             00000014             00000000          00000000       00000014        0       0        0         0         0       0         1           0     6              1  0
2100 ns  1    1    2042000a     0000001c         02                  02                  02                 0000000a            00000000             00000000             0000000a          00000000       00000000        1       0        0         0         0       1         0           0     2              0  0
\end{verbatim}
\endgroup
\end{landscape}
\restoregeometry

\newpage
\chapter{Conclusions}
The extensively tests performed on each module and onto the final design of the architecture have demonstrated the correctness of its functioning.\\
However, I think there are some improvements that could be done, for example:
\begin{itemize}
    \item modify the architecture in order to model the delays for the real components
    \item try to integrate the floating point unit (FPU) developed by Francesco Tumaini\footnote{\url{https://bitbucket.org/ftumaini/fpu_project}}(not done due to a lack of time)
    \item provide a graphical simulation with waveforms for the components (I've tried to use gtkwave, it works fine for small components but it seems it doesn't handle very well array signals of sc\_logic type).
\end{itemize}

\newpage
\addcontentsline{toc}{chapter}{Bibliography}
\bibliography{biblio}{}
\bibliographystyle{unsrt}



\end{document}
