\select@language {english}
\contentsline {chapter}{Abstract}{1}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {chapter}{\numberline {2}Architecture overview}{5}
\contentsline {chapter}{\numberline {3}The Datapath}{7}
\contentsline {section}{\numberline {3.1}Logic gates}{7}
\contentsline {section}{\numberline {3.2}Full-adder}{7}
\contentsline {section}{\numberline {3.3}Multiplexer}{8}
\contentsline {section}{\numberline {3.4}Arithmetic Logic Unit (ALU)}{9}
\contentsline {subsection}{\numberline {3.4.1}1 bit ALU}{9}
\contentsline {subsection}{\numberline {3.4.2}32 bit ALU}{10}
\contentsline {section}{\numberline {3.5}Shifter}{11}
\contentsline {section}{\numberline {3.6}Sign Extender}{11}
\contentsline {section}{\numberline {3.7}Decoder}{12}
\contentsline {section}{\numberline {3.8}Register}{12}
\contentsline {section}{\numberline {3.9}Register file}{13}
\contentsline {subsection}{\numberline {3.9.1}The read portion}{13}
\contentsline {subsection}{\numberline {3.9.2}The write portion}{13}
\contentsline {section}{\numberline {3.10}Memory units}{14}
\contentsline {subsection}{\numberline {3.10.1}Instruction memory}{14}
\contentsline {subsection}{\numberline {3.10.2}Data memory}{15}
\contentsline {chapter}{\numberline {4}The Control Unit}{17}
\contentsline {section}{\numberline {4.1}Control}{17}
\contentsline {section}{\numberline {4.2}ALU Control}{18}
\contentsline {chapter}{\numberline {5}Top-level view}{19}
\contentsline {chapter}{\numberline {6}Tests}{21}
\contentsline {chapter}{\numberline {7}Conclusions}{25}
\contentsline {chapter}{Bibliography}{26}
