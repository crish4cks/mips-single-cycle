#include "alu32.hpp"

void alu32::update_res() {
    for(unsigned i = 0; i < 32; i++)
        RES[i].write( N1[i].read() );  
}

void alu32::update_cout() {
    COUT.write( C[31].read() );  
}