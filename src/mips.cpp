#include "mips.hpp"

void mips::update_alusrc() {  
    ALUSRC.write( ALSRC0.read() );
}

void mips::update_memread() {
    MEMREAD.write( MR0.read() );    
}

void mips::update_memwrite() {
    MEMWRITE.write( MW0.read() );  
}

void mips::update_memtoreg() {
    MEMTOREG.write( MTR0.read() );    
}

void mips::update_regdst() {
    REGDST.write( RD0.read() );     
}

void mips::update_regwrite() {
    REGWRITE.write( RW0.read() );  
}

void mips::update_branchsel() {
    BRANCH_SEL.write( BSEL0.read() );  
}

void mips::update_jump() {
    JUMP.write( J0.read() );  
}

void mips::update_z() {
    Z.write( Z0.read() );  
}

void mips::update_alu_operation() {
    for(unsigned i = 0; i < 4; i++)
        ALU_OPERATION[i].write( OPR0[i].read() );
}
