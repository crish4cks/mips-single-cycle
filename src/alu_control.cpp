#include "alu_control.hpp"

void alu_control::select_operation() {
    sc_lv<2> aluop;
    sc_lv<6> function_code;
    sc_lv<4> operation;
    
    aluop[1] = ALUOP[1];
    aluop[0] = ALUOP[0];
    
    for(unsigned i = 0; i < 6; i++)
        function_code[i] = FUNCTION_CODE[i].read();
    
    if(aluop == "00")
        operation = "0010";
    else if(aluop == "01")
        operation = "0110";
    else if(aluop == "10" && function_code.range(3,0) == "0000")
	operation = "0010";
    else if(aluop[1] == '1' && function_code.range(3,0) == "0010")
        operation = "0110";
    else if(aluop == "10" && function_code.range(3,0) == "0100")
        operation = "0000";
    else if(aluop == "10" && function_code.range(3,0) == "0101")
        operation = "0001";
    else if(aluop[1] == '1' && function_code.range(3,0) == "1010")
        operation = "0111";
    
    for(unsigned i = 0; i < 4; i++)
        OPERATION[i].write(operation[i]);
}