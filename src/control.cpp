#include "control.hpp"

void control::compute_control_signals() {
    sc_lv<6> opcode;
    
    for(unsigned i = 0; i < 6; i++)
        opcode[i] = OPCODE[i].read();
    
    if(opcode == "000000") {          // R-type (0)
        REGDST.write(SC_LOGIC_1);  
	REGWRITE.write(SC_LOGIC_1);
	ALUSRC.write(SC_LOGIC_0);
	ALUOP[1].write(SC_LOGIC_1);
	ALUOP[0].write(SC_LOGIC_0);
	MEMTOREG.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_0);
	MEMWRITE.write(SC_LOGIC_0);
	BRANCH.write(SC_LOGIC_0);
	JUMP.write(SC_LOGIC_0);
      
    }
    else if(opcode == "001000") {     // addi (8)
        REGDST.write(SC_LOGIC_0);  
	REGWRITE.write(SC_LOGIC_1);
	ALUSRC.write(SC_LOGIC_1);
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_0);
	MEMTOREG.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_0);
	MEMWRITE.write(SC_LOGIC_0);
	BRANCH.write(SC_LOGIC_0);
	JUMP.write(SC_LOGIC_0);  
      
    }
    else if(opcode == "100011") {     // lw (35)
        REGDST.write(SC_LOGIC_0);  
	REGWRITE.write(SC_LOGIC_1);
	ALUSRC.write(SC_LOGIC_1);
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_0);
	MEMTOREG.write(SC_LOGIC_1);
	MEMREAD.write(SC_LOGIC_1);
	MEMWRITE.write(SC_LOGIC_0);
	BRANCH.write(SC_LOGIC_0);
	JUMP.write(SC_LOGIC_0);  
      
    }
    else if(opcode == "101011") {     // sw (43)
        REGDST.write(SC_LOGIC_0);  
	REGWRITE.write(SC_LOGIC_0);
	ALUSRC.write(SC_LOGIC_1);
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_0);
	MEMTOREG.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_0);
	MEMWRITE.write(SC_LOGIC_1);
	BRANCH.write(SC_LOGIC_0);
	JUMP.write(SC_LOGIC_0);  
      
    }
    else if(opcode == "000100") {     // beq (4)
        REGDST.write(SC_LOGIC_0);  
	REGWRITE.write(SC_LOGIC_0);
	ALUSRC.write(SC_LOGIC_0);
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_1);
	MEMTOREG.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_0);
	MEMWRITE.write(SC_LOGIC_0);
	BRANCH.write(SC_LOGIC_1);
	JUMP.write(SC_LOGIC_0);  
      
    }
    else if(opcode == "000010") {     // j (2)
        REGDST.write(SC_LOGIC_0);  
	REGWRITE.write(SC_LOGIC_0);
	ALUSRC.write(SC_LOGIC_0);
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_1);
	MEMTOREG.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_0);
	MEMWRITE.write(SC_LOGIC_0);
	BRANCH.write(SC_LOGIC_0);
	JUMP.write(SC_LOGIC_1);  
      
    }
    else {                            // other cases
        REGDST.write(SC_LOGIC_X);  
	REGWRITE.write(SC_LOGIC_X);
	ALUSRC.write(SC_LOGIC_X);
	ALUOP[1].write(SC_LOGIC_X);
	ALUOP[0].write(SC_LOGIC_X);
	MEMTOREG.write(SC_LOGIC_X);
	MEMREAD.write(SC_LOGIC_X);
	MEMWRITE.write(SC_LOGIC_X);
	BRANCH.write(SC_LOGIC_X);
	JUMP.write(SC_LOGIC_X);  
    }
    
}