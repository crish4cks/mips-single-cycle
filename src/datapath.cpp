#include "datapath.hpp"

void datapath::update_opcode() {
    for(unsigned i = 26; i < 32; i++)
        OPCODE[i - 26].write( I0[i].read() );
} 

void datapath::update_funct() {
    for(unsigned i = 0; i < 6; i++)
        FUNCT[i].write( I0[i].read() );
}

void datapath::update_program_counter() {
    for(unsigned i = 0; i < 32; i++)
        PROGRAM_COUNTER[i].write( P0[i].read() );
}

void datapath::update_instruction() {
    for(unsigned i = 0; i < 32; i++)
        INSTRUCTION[i].write( I0[i].read() );
}

void datapath::update_regfile_read_reg_1() {
    for(unsigned i = 0; i < 5; i++)
        REGFILE_READ_REG_1[i].write( I0[i + 21].read() );
}

void datapath::update_regfile_read_reg_2() {
    for(unsigned i = 0; i < 5; i++)
        REGFILE_READ_REG_2[i].write( I0[i + 16].read() );
}

void datapath::update_regfile_write_reg() {
    for(unsigned i = 0; i < 5; i++)
        REGFILE_WRITE_REG[i].write( M0[i].read() );
}

void datapath::update_regfile_write_data() {
    for(unsigned i = 0; i < 32; i++)
        REGFILE_WRITE_DATA[i].write( M4[i].read() );
}

void datapath::update_regfile_read_data_1() {
    for(unsigned i = 0; i < 32; i++)
        REGFILE_READ_DATA_1[i].write( R0[i].read() );
}

void datapath::update_regfile_read_data_2() {
    for(unsigned i = 0; i < 32; i++)
        REGFILE_READ_DATA_2[i].write( R1[i].read() );
}

void datapath::update_mem_data_address() {
    for(unsigned i = 0; i < 32; i++)
        MEM_DATA_ADDRESS[i].write( AL0[i].read() );
}

void datapath::update_mem_read_data() {
    for(unsigned i = 0; i < 32; i++)
        MEM_READ_DATA[i].write( DT0[i].read() );
}

void datapath::update_mem_write_data() {
    for(unsigned i = 0; i < 32; i++)
        MEM_WRITE_DATA[i].write( R1[i].read() );
}
