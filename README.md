### What is this repository for? ###

* A very simple implementation of the MIPS Single Cycle 32 bit architecture
* Version: 0.1 (stable)

### What do I need to compile/run the code? ###
* A Linux distro (i.e. Debian GNU/Linux x86_64)
* a C/C++ compiler (gcc/g++)
* the [ SystemC library ](http://accellera.org/downloads/standards/systemc)
* cmake
* git
* wget (optional)

### Setup ###

#### How to install the SystemC library ####
Download the library:

    wget http://accellera.org/images/downloads/standards/systemc/systemc-2.3.1.tgz

Unpack:

    tar -zxvf systemc-2.3.1.tgz

Build and install:

    cd systemc-2.3.1
    mkdir build && cd build
    ../configure
    make
    sudo make install
    sudo cp -R ../include/* /usr/include
    sudo cp ../lib-linux64/libsystemc* /usr/lib    


#### Download, compile and run the code in this repo ####

At this time there are two main branches: master and testing.
The first one corresponds to the 'stable' version but it could be not up to date, so if you want to try the last changes, I suggest you to choose the 'current' version.

Download the 'stable' version:

    git clone https://crish4cks@bitbucket.org/crish4cks/mips-single-cycle.git

Download the 'current' version:

    git clone -b testing https://crish4cks@bitbucket.org/crish4cks/mips-single-cycle.git

Build the sources:
    
    cd mips-single-cycle/
    mkdir build && cd build
    cmake ..
    make

Test all the stuff:

    make test

At this point in the current directory (~/mips-single-cycle/build) you will find some binaries. You can execute each of them by adding ./ before the name of the executable. For example, let's suppose you want to run the testbench for the multiplexer:

    ./tb_mux

In order to execute the MIPS simulation you must launch the corresponding testbench:

    ./tb_mips

For the simulation will be used the instructions present into the file called "rom.txt" (located in ~/mips-single-cycle/test/data). The result of the simulation will be written into "sim.log" (into the current directory).
By default a simple test is already present (for details see the doc), but you can simulate your own program by overwriting the content of rom.txt.

If you want to keep clean the build directory, you can use this simple bash script:
    
    # Clean the 'build' directory
    # 
    #!/bin/bash
    rm -rf CMakeCache.txt \
           CMakeFiles \
           cmake_install.cmake \
           CTestTestfile.cmake \
           *.a \
           Makefile \
           tb_* \
           Testing \
           *.vcd \
           sim.log

### Who do I talk to? ###

* Repo owner/admin