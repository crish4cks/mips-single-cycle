#include <string>
#include "data_memory.hpp"
#include "stimulus_data_memory.hpp"
#include "checker_data_memory.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> RSTsig, ADDRESSsig[32], WRITE_DATAsig[32], MEMREADsig, MEMWRITEsig;
    sc_signal<sc_logic> READ_DATAsig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    data_memory<5> DUT("DataMemory");           
                   
    // Assign signals
    for(unsigned i = 0; i < 32; i++) {
        DUT.ADDRESS[i](ADDRESSsig[i]); 	
	DUT.WRITE_DATA[i](WRITE_DATAsig[i]);
	DUT.READ_DATA[i](READ_DATAsig[i]);
    }
    DUT.CLK(TestCLK);
    DUT.RST(RSTsig);
    DUT.MEMREAD(MEMREADsig);
    DUT.MEMWRITE(MEMWRITEsig);
    
    
    // Check the number of arguments passed
    if(argc != 6) {
      
        // Instantiate the stimulus generator
        stimulus_data_memory stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            stim.ADDRESS[i](ADDRESSsig[i]);
	    stim.WRITE_DATA[i](WRITE_DATAsig[i]);
	}
	stim.RST(RSTsig);
	stim.MEMREAD(MEMREADsig);
        stim.MEMWRITE(MEMWRITEsig);
      	
	// Instantiate the checker
        checker_data_memory chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            chk.ADDRESS[i](ADDRESSsig[i]); 	
	    chk.WRITE_DATA[i](WRITE_DATAsig[i]);
	    chk.READ_DATA[i](READ_DATAsig[i]);
        }
        chk.RST(RSTsig);    
	chk.MEMREAD(MEMREADsig);
        chk.MEMWRITE(MEMWRITEsig);
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("DataMemory");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	sc_trace(fp, RSTsig, "RST");
	sc_trace(fp, MEMREADsig, "MEMREAD");
	sc_trace(fp, MEMWRITEsig, "MEMWRITE");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, ADDRESSsig[i], "ADDRESS(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, WRITE_DATAsig[i], "WRITE_DATA(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, READ_DATAsig[i], "READ_DATA");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        RSTsig.write(sc_logic(argv[1][0]));
	MEMREADsig.write(sc_logic(argv[2][0]));
	MEMWRITEsig.write(sc_logic(argv[3][0]));
      
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++) {
            ADDRESSsig[i].write(sc_logic(argv[4][j]));
	    WRITE_DATAsig[i].write(sc_logic(argv[5][j]));
	}
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> address_sig = argv[4];	
	sc_lv<32> write_data_sig = argv[5];
	
        if(address_sig == sc_lv<32>("00000000000000000000000000000000") && write_data_sig == sc_lv<32>("00000000000000000000000010111010") && RSTsig.read() == '1' && MEMREADsig.read() == '0' && MEMWRITEsig.read() == '1') {
	    sc_lv<32> read_data = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(READ_DATAsig[i].read() != read_data[i])
		    return 1;  
            return 0;	    
	}
	
	if(address_sig == sc_lv<32>("00000000000000000000000000000000") && write_data_sig == sc_lv<32>("00000000000000000000000110111010") && RSTsig.read() == '0' && MEMREADsig.read() == '1' && MEMWRITEsig.read() == '0') {
	    sc_lv<32> read_data = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(READ_DATAsig[i].read() != read_data[i])
		    return 1;  
            return 0;	    
	}

    }           
    
    return 0;
    
}
