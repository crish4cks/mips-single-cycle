#include <string>
#include "reg.hpp"
#include "stimulus_reg.hpp"
#include "checker_reg.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> RSTsig, LOADsig, Dsig[32];
    sc_signal<sc_logic> Qsig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    reg<32> DUT("Reg32");           
                   
    // Assign signals
    for(unsigned i = 0; i < 32; i++) {
        DUT.D[i](Dsig[i]); 	
	DUT.Q[i](Qsig[i]);
    }
    DUT.CLK(TestCLK);
    DUT.RST(RSTsig);
    DUT.LOAD(LOADsig);
    
    
    // Check the number of arguments passed
    if(argc != 4) {
      
        // Instantiate the stimulus generator
        stimulus_reg stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++)
            stim.D[i](Dsig[i]); 	    
	stim.RST(RSTsig);
	stim.LOAD(LOADsig);
      	
	// Instantiate the checker
        checker_reg chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            chk.D[i](Dsig[i]); 
	    chk.Q[i](Qsig[i]);
        }
        chk.RST(RSTsig);    
	chk.LOAD(LOADsig);
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("reg32");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	sc_trace(fp, RSTsig, "RST");
	sc_trace(fp, LOADsig, "LOAD");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Dsig[i], "D(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, Qsig[i], "Q");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++)
            Dsig[i].write(sc_logic(argv[1][j]));
	
	RSTsig.write(sc_logic(argv[2][0]));
	LOADsig.write(sc_logic(argv[3][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> dsig = argv[1];	
	
        if(dsig == sc_lv<32>("00000000000000000000000000010000") && RSTsig.read() == '1' && LOADsig.read() == '1') {
	    sc_lv<32> q = "00000000000000000000000000010000";
            for(unsigned i = 0; i < 32; i++)
	        if(Qsig[i].read() != q[i])
		    return 1;  
            return 0;	    
	}
	
	// It cannot work because we should assume to have a previous value for the output. Therefore we won't use this test.
	//if(dsig == sc_lv<32>("00000000000000000000000000011010") && RSTsig.read() == '1' && LOADsig.read() == '0') {
	//    sc_lv<32> q = "00000000000000000000000000010000";
        //    for(unsigned i = 0; i < 32; i++)
	//        if(Qsig[i].read() != q[i])
	//	    return 1;  
        //    return 0;	    
	//}
	
	if(dsig == sc_lv<32>("00000000000000000000000010101010") && RSTsig.read() == '1' && LOADsig.read() == '1') {
	    sc_lv<32> q = "00000000000000000000000010101010";
            for(unsigned i = 0; i < 32; i++)
	        if(Qsig[i].read() != q[i])
		    return 1;  
            return 0;	    
	}
	
	if(dsig == sc_lv<32>("00000000000000000000000010101011") && RSTsig.read() == '1' && LOADsig.read() == '1') {
	    sc_lv<32> q = "00000000000000000000000010101011";
            for(unsigned i = 0; i < 32; i++)
	        if(Qsig[i].read() != q[i])
		    return 1;  
            return 0;	    
	}
	
	if(dsig == sc_lv<32>("00000000000000000000000010101010") && RSTsig.read() == '0' && LOADsig.read() == '0') {
	    sc_lv<32> q = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(Qsig[i].read() != q[i])
		    return 1;  
            return 0;	    
	}
	
	if(dsig == sc_lv<32>("00000000000000000000000010101010") && RSTsig.read() == '0' && LOADsig.read() == '1') {
	    sc_lv<32> q = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(Qsig[i].read() != q[i])
		    return 1;  
            return 0;	    
	}

    }           
    
    return 0;
    
}
