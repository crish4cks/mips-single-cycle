#include <string>
#include "control.hpp"
#include "stimulus_control.hpp"
#include "checker_control.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> OPCODEsig[6];
    sc_signal<sc_logic> ALUOPsig[2], ALUSRCsig, MEMREADsig, MEMWRITEsig, MEMTOREGsig, REGDSTsig, REGWRITEsig, BRANCHsig, JUMPsig;
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    control DUT("Control");           
                   
    // Assign signals
    for(unsigned i = 0; i < 6; i++)
        DUT.OPCODE[i](OPCODEsig[i]);
    DUT.ALUOP[1](ALUOPsig[1]);
    DUT.ALUOP[0](ALUOPsig[0]);
    DUT.ALUSRC(ALUSRCsig);
    DUT.MEMREAD(MEMREADsig);
    DUT.MEMWRITE(MEMWRITEsig);
    DUT.MEMTOREG(MEMTOREGsig);
    DUT.REGDST(REGDSTsig);
    DUT.REGWRITE(REGWRITEsig);
    DUT.BRANCH(BRANCHsig);
    DUT.JUMP(JUMPsig);
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_control stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
        for(unsigned i = 0; i < 6; i++)
            stim.OPCODE[i](OPCODEsig[i]);
      	
	// Instantiate the checker
        checker_control chk("Checker");
    
        // Assign signals
	chk.CLK(TestCLK);
        for(unsigned i = 0; i < 6; i++)
            chk.OPCODE[i](OPCODEsig[i]);
	chk.ALUOP[1](ALUOPsig[1]);
        chk.ALUOP[0](ALUOPsig[0]);
	chk.ALUSRC(ALUSRCsig);
        chk.MEMREAD(MEMREADsig);
        chk.MEMWRITE(MEMWRITEsig);
        chk.MEMTOREG(MEMTOREGsig);
        chk.REGDST(REGDSTsig);
	chk.REGWRITE(REGWRITEsig);
        chk.BRANCH(BRANCHsig);
        chk.JUMP(JUMPsig);
	
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("control");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");	
	for(int i = 5; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, OPCODEsig[i], "OPCODE(" + string(ch) + ")");	    
	}
	sc_trace(fp, ALUOPsig[1], "ALUOP(1)");
	sc_trace(fp, ALUOPsig[0], "ALUOP(0)");
	sc_trace(fp, ALUSRCsig, "ALUOSRC");
	sc_trace(fp, MEMREADsig, "MEMREAD");
	sc_trace(fp, MEMWRITEsig, "MEMWRITE");
	sc_trace(fp, MEMTOREGsig, "MEMTOREG");
	sc_trace(fp, REGDSTsig, "REGDST");
	sc_trace(fp, REGWRITEsig, "REGWRITE");
	sc_trace(fp, BRANCHsig, "BRANCH");
	sc_trace(fp, JUMPsig, "JUMP");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
      
        for(int i = 5, j = 0; i >= 0, j < 6; i--, j++)
            OPCODEsig[i].write(sc_logic(argv[1][j]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<6> opcode = argv[1];
	
        if(opcode == sc_lv<6>("000000")) {
	    if(ALUOPsig[1].read() == '1' && ALUOPsig[0].read() == '0' && ALUSRCsig.read() == '0' && MEMREADsig.read() == '0' && MEMWRITEsig.read() == '0' && MEMTOREGsig.read() == '0' && REGDSTsig.read() == '1' && REGWRITEsig.read() == '1' && BRANCHsig.read() == '0' && JUMPsig.read() == '0')  
                return 0;
	    else return 1;
	}
	
	if(opcode == sc_lv<6>("100011")) {
	    if(ALUOPsig[1].read() == '0' && ALUOPsig[0].read() == '0' && ALUSRCsig.read() == '1' && MEMREADsig.read() == '1' && MEMWRITEsig.read() == '0' && MEMTOREGsig.read() == '1' && REGDSTsig.read() == '0' && REGWRITEsig.read() == '1' && BRANCHsig.read() == '0' && JUMPsig.read() == '0')  
                return 0;
	    else return 1;
	}

	if(opcode == sc_lv<6>("101011")) {
	    if(ALUOPsig[1].read() == '0' && ALUOPsig[0].read() == '0' && ALUSRCsig.read() == '1' && MEMREADsig.read() == '0' && MEMWRITEsig.read() == '1' && MEMTOREGsig.read() == '0' && REGDSTsig.read() == '0' && REGWRITEsig.read() == '0' && BRANCHsig.read() == '0' && JUMPsig.read() == '0')  
                return 0;
	    else return 1;
	}
	
	if(opcode == sc_lv<6>("000100")) {
	    if(ALUOPsig[1].read() == '0' && ALUOPsig[0].read() == '1' && ALUSRCsig.read() == '0' && MEMREADsig.read() == '0' && MEMWRITEsig.read() == '0' && MEMTOREGsig.read() == '0' && REGDSTsig.read() == '0' && REGWRITEsig.read() == '0' && BRANCHsig.read() == '1' && JUMPsig.read() == '0')  
                return 0;
	    else return 1;
	}
	if(opcode == sc_lv<6>("111111")) {
	    if(ALUOPsig[1].read() == 'X' && ALUOPsig[0].read() == 'X' && ALUSRCsig.read() == 'X' && MEMREADsig.read() == 'X' && MEMWRITEsig.read() == 'X' && MEMTOREGsig.read() == 'X' && REGDSTsig.read() == 'X' && REGWRITEsig.read() == 'X' && BRANCHsig.read() == 'X' && JUMPsig.read() == 'X')  
                return 0;
	    else return 1;
	}
	

    }           
    
    return 0;
    
}
