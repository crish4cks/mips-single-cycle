#include <string>
#include "xor_gate.hpp"
#include "stimulus_xor_gate.hpp"
#include "checker_xor_gate.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    string str("");
    char ch[3];
    
    // Stimulus signals
    sc_signal<sc_logic> Asig[3];
    sc_signal<sc_logic> Ysig;
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    xor_gate<3> DUT("XorGate");           
                   
    // Assign signals
    for(unsigned i = 0; i < 3; i++)
        DUT.A[i](Asig[i]);                             
    DUT.Y(Ysig);
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_xor_gate stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 3; i++)
            stim.A[i](Asig[i]);
      
	// Instantiate the checker
        checker_xor_gate chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 3; i++)
            chk.A[i](Asig[i]);                             
        chk.Y(Ysig);
    
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("xor_gate");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
        for(int i = 2; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Asig[i], "A(" + string(ch) + ")");
	}
        sc_trace(fp, Ysig, "Y");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 2, j = 0; i >= 0, j < 3; i--, j++)
            Asig[i].write(sc_logic(argv[1][j]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<3> asig = argv[1];
	
        if(asig == sc_lv<3>("000"))
            return !(Ysig.read() == '0');

	if(asig == sc_lv<3>("001"))
            return !(Ysig.read() == '1');
	
	if(asig == sc_lv<3>("010"))
            return !(Ysig.read() == '1');
	
	if(asig == sc_lv<3>("011"))
            return !(Ysig.read() == '0');
	
	if(asig == sc_lv<3>("100"))
            return! (Ysig.read() == '1');
	
	if(asig == sc_lv<3>("101"))
            return !(Ysig.read() == '0');
          
        if(asig == sc_lv<3>("110"))
            return !(Ysig.read() == '0');
	
	if(asig == sc_lv<3>("111"))
            return !(Ysig.read() == '1');
	
    }           

    return 0;
    
}

