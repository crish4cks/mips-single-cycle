#include <string>
#include "alu_control.hpp"
#include "stimulus_alu_control.hpp"
#include "checker_alu_control.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> ALUOPsig[2], FUNCTION_CODEsig[6];
    sc_signal<sc_logic> OPERATIONsig[4];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    alu_control DUT("AluControl");           
                   
    // Assign signals
    DUT.ALUOP[1](ALUOPsig[1]);
    DUT.ALUOP[0](ALUOPsig[0]);
    for(unsigned i = 0; i < 6; i++)
        DUT.FUNCTION_CODE[i](FUNCTION_CODEsig[i]);
    for(unsigned i = 0; i < 4; i++)
        DUT.OPERATION[i](OPERATIONsig[i]);
    
    
    // Check the number of arguments passed
    if(argc != 3) {
      
        // Instantiate the stimulus generator
        stimulus_alu_control stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	stim.ALUOP[1](ALUOPsig[1]);
        stim.ALUOP[0](ALUOPsig[0]);
        for(unsigned i = 0; i < 6; i++)
            stim.FUNCTION_CODE[i](FUNCTION_CODEsig[i]);
      	
	// Instantiate the checker
        checker_alu_control chk("Checker");
    
        // Assign signals
	chk.CLK(TestCLK);
        chk.ALUOP[1](ALUOPsig[1]);
        chk.ALUOP[0](ALUOPsig[0]);
        for(unsigned i = 0; i < 6; i++)
            chk.FUNCTION_CODE[i](FUNCTION_CODEsig[i]);
	for(unsigned i = 0; i < 4; i++)
            chk.OPERATION[i](OPERATIONsig[i]);
	
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("alu_control");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	sc_trace(fp, ALUOPsig[1], "ALUOP(1)");
	sc_trace(fp, ALUOPsig[0], "ALUOP(0)");
	for(int i = 5; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, FUNCTION_CODEsig[i], "FUNCTION_CODE(" + string(ch) + ")");	    
	}
	for(int i = 3; i >= 0; i--)
	    sc_trace(fp, OPERATIONsig[i], "OPERATION(" + string(ch) + ")");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
      
        ALUOPsig[1].write(sc_logic(argv[1][0]));
	ALUOPsig[0].write(sc_logic(argv[1][1]));
        for(int i = 5, j = 0; i >= 0, j < 6; i--, j++)
            FUNCTION_CODEsig[i].write(sc_logic(argv[2][j]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<2> aluop = argv[1];	
	sc_lv<6> function_code = argv[2];
	
        if(aluop == sc_lv<2>("01") && function_code == sc_lv<6>("110000")) {
	    sc_lv<4> operation = "0110";
            for(unsigned i = 0; i < 4; i++)
	        if(OPERATIONsig[i].read() != operation[i])
		    return 1;  
            return 0;	    
	}
	
	if(aluop == sc_lv<2>("10") && function_code == sc_lv<6>("100010")) {
	    sc_lv<4> operation = "0110";
            for(unsigned i = 0; i < 4; i++)
	        if(OPERATIONsig[i].read() != operation[i])
		    return 1;  
            return 0;	    
	}
	
	if(aluop == sc_lv<2>("10") && function_code == sc_lv<6>("101010")) {
	    sc_lv<4> operation = "0111";
            for(unsigned i = 0; i < 4; i++)
	        if(OPERATIONsig[i].read() != operation[i])
		    return 1;  
            return 0;	    
	}
	

    }           
    
    return 0;
    
}
