#include "alu.hpp"
#include "stimulus_alu.hpp"
#include "checker_alu.hpp"

int sc_main(int argc, char* argv[]) {
  
    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);
    
    // Stimulus signals
    sc_signal<sc_logic> Asig, Bsig, CINsig, LESSsig, A_INVsig, B_INVsig, RESsig, SETsig, COUTsig;
    sc_signal<sc_logic> OPERATIONsig[2];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    alu DUT("Alu");           
                   
    // Assign signals
    DUT.A(Asig);                             
    DUT.B(Bsig);
    DUT.CIN(CINsig);
    DUT.LESS(LESSsig);
    DUT.A_INV(A_INVsig);
    DUT.B_INV(B_INVsig);
    DUT.OPERATION[0](OPERATIONsig[0]);
    DUT.OPERATION[1](OPERATIONsig[1]);
    DUT.RES(RESsig);
    DUT.SET(SETsig);
    DUT.COUT(COUTsig);
    
    
    // Check the number of arguments passed
    if(argc != 8) {
        
        // Instantiate the stimulus generator
        stimulus_alu stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
        stim.A(Asig);
        stim.B(Bsig);
	stim.LESS(LESSsig);
        stim.A_INV(A_INVsig);
        stim.B_INV(B_INVsig);
	stim.OPERATION[0](OPERATIONsig[0]);
        stim.OPERATION[1](OPERATIONsig[1]);
        stim.CIN(CINsig);
      
	
	// Instantiate the checker
        checker_alu chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
        chk.A(Asig);                             
        chk.B(Bsig);
        chk.CIN(CINsig);
        chk.LESS(LESSsig);
        chk.A_INV(A_INVsig);
        chk.B_INV(B_INVsig);
        chk.OPERATION[0](OPERATIONsig[0]);
        chk.OPERATION[1](OPERATIONsig[1]);
        chk.RES(RESsig);
        chk.SET(SETsig);
        chk.COUT(COUTsig);
	
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("alu");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
        sc_trace(fp, Asig, "A");                  
        sc_trace(fp, Bsig, "B");
        sc_trace(fp, CINsig, "CIN");
	sc_trace(fp, LESSsig, "LESS");
	sc_trace(fp, A_INVsig, "A_INV");
	sc_trace(fp, B_INVsig, "B_INV");
	sc_trace(fp, OPERATIONsig[1], "OPERATION(1)");
	sc_trace(fp, OPERATIONsig[0], "OPERATION(0)");
        sc_trace(fp, RESsig, "RES");
	sc_trace(fp, SETsig, "SET");
        sc_trace(fp, COUTsig, "COUT");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        
        Asig.write(sc_logic(argv[1][0]));
        Bsig.write(sc_logic(argv[2][0]));
        CINsig.write(sc_logic(argv[3][0]));  
	LESSsig.write(sc_logic(argv[4][0]));
        A_INVsig.write(sc_logic(argv[5][0]));
        B_INVsig.write(sc_logic(argv[6][0]));
	OPERATIONsig[0].write(sc_logic(argv[7][1]));
	OPERATIONsig[1].write(sc_logic(argv[7][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<2> operation = argv[7];
	
        if(Asig.read() == '0' && Bsig.read() == '1' && CINsig.read() == '0' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '0' && operation == sc_lv<2>("10"))
	    return !(RESsig.read() == '1' && SETsig.read() == '1' && COUTsig.read() == '0');

        if(Asig.read() == '1' && Bsig.read() == '1' && CINsig.read() == '1' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '1' && operation == sc_lv<2>("10"))
	    return !(RESsig.read() == '0' && SETsig.read() == '0' && COUTsig.read() == '1');
	
	if(Asig.read() == '1' && Bsig.read() == '0' && CINsig.read() == '0' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '0' && operation == sc_lv<2>("01"))
	    return !(RESsig.read() == '1' && SETsig.read() == '1' && COUTsig.read() == '0');
	
	if(Asig.read() == '1' && Bsig.read() == '0' && CINsig.read() == '0' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '0' && operation == sc_lv<2>("00"))
	    return !(RESsig.read() == '0' && SETsig.read() == '1' && COUTsig.read() == '0');
	
	if(Asig.read() == '1' && Bsig.read() == '1' && CINsig.read() == '0' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '0' && operation == sc_lv<2>("11"))
	    return !(RESsig.read() == '0' && SETsig.read() == '0' && COUTsig.read() == '1');
	
	if(Asig.read() == '0' && Bsig.read() == '1' && CINsig.read() == '1' && LESSsig.read() == '0' && A_INVsig.read() == '0' && B_INVsig.read() == '1' && operation == sc_lv<2>("10"))
	    return !(RESsig.read() == '1' && SETsig.read() == '1' && COUTsig.read() == '0');
	
	if(Asig.read() == '0' && Bsig.read() == '1' && CINsig.read() == '1' && LESSsig.read() == '1' && A_INVsig.read() == '0' && B_INVsig.read() == '1' && operation == sc_lv<2>("11"))
	    return !(RESsig.read() == '1' && SETsig.read() == '1' && COUTsig.read() == '0');
              
    }           

    return 0;
    
}

