#include <string>
#include "instruction_memory.hpp"
#include "stimulus_instruction_memory.hpp"
#include "checker_instruction_memory.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> ADDRESSsig[32], DATAsig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    instruction_memory<5> DUT("InstructionMemory");           
                   
    // Assign signals
    for(unsigned i = 0; i < 32; i++) {
        DUT.ADDRESS[i](ADDRESSsig[i]); 	
	DUT.DATA[i](DATAsig[i]);
    }
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_instruction_memory stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++)
            stim.ADDRESS[i](ADDRESSsig[i]);
      	
	// Instantiate the checker
        checker_instruction_memory chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            chk.ADDRESS[i](ADDRESSsig[i]); 	
	    chk.DATA[i](DATAsig[i]);
        }
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("InstructionMemory");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, ADDRESSsig[i], "ADDRESS(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, DATAsig[i], "DATA");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
      
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++)
            ADDRESSsig[i].write(sc_logic(argv[1][j]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> address_sig = argv[1];		
	
        if(address_sig == sc_lv<32>("00000000000000000000000000000000")) {
	    sc_lv<32> data = "00100010010100100000000000000101";
            for(unsigned i = 0; i < 32; i++)
	        if(DATAsig[i].read() != data[i])
		    return 1;  
            return 0;	    
	}
	
	if(address_sig == sc_lv<32>("00000000000000000000000000000100")) {
	    sc_lv<32> data = "00100001010010100000000000010100";
            for(unsigned i = 0; i < 32; i++)
	        if(DATAsig[i].read() != data[i])
		    return 1;  
            return 0;	    
	}
	
        if(address_sig == sc_lv<32>("00000000000000000000000000001000")) {
	    sc_lv<32> data = "10001101001010000000000000001100";
            for(unsigned i = 0; i < 32; i++)
	        if(DATAsig[i].read() != data[i])
		    return 1;  
            return 0;	    
	}
	
	if(address_sig == sc_lv<32>("00000000000000000000000000001100")) {
	    sc_lv<32> data = "00000010010010000100000000100000";
            for(unsigned i = 0; i < 32; i++)
	        if(DATAsig[i].read() != data[i])
		    return 1;  
            return 0;	    
	}
	
    }           
    
    return 0;
    
}
