#include "full_adder.hpp"
#include "stimulus_full_adder.hpp"
#include "checker_full_adder.hpp"

int sc_main(int argc, char* argv[]) {
    
    // Stimulus signals
    sc_signal< sc_logic > Asig, Bsig, CINsig, Ysig, COUTsig;
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    full_adder DUT("FullAdder");           
                   
    // Assign signals
    DUT.A(Asig);                             
    DUT.B(Bsig);
    DUT.CIN(CINsig);
    DUT.Y(Ysig);
    DUT.COUT(COUTsig);
    
    
    // Check the number of arguments passed
    if(argc != 4) {
        
        // Instantiate the stimulus generator
        stimulus_full_adder stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
        stim.A(Asig);
        stim.B(Bsig);
        stim.CIN(CINsig);
      
	
	// Instantiate the checker
        checker_full_adder chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
        chk.A(Asig);                             
        chk.B(Bsig);
        chk.CIN(CINsig);
        chk.Y(Ysig);
        chk.COUT(COUTsig);
	
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("full_adder");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
        sc_trace(fp, Asig, "A");                  
        sc_trace(fp, Bsig, "B");
        sc_trace(fp, CINsig, "CIN");
        sc_trace(fp, Ysig, "Y");
        sc_trace(fp, COUTsig, "COUT");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        
        Asig.write(sc_logic(argv[1][0]));
        Bsig.write(sc_logic(argv[2][0]));
        CINsig.write(sc_logic(argv[3][0]));  
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
        if(Asig.read() == '0' && Bsig.read() == '0' && CINsig.read() == '0')
	    return !(Ysig.read() == '0' && COUTsig.read() == '0');

        if(Asig.read() == '0' && Bsig.read() == '0' && CINsig.read() == '1')
            return !(Ysig.read() == '1' && COUTsig.read() == '0');

        if(Asig.read() == '0' && Bsig.read() == '1' && CINsig.read() == '0')
            return !(Ysig.read() == '1' && COUTsig.read() == '0');

        if(Asig.read() == '0' && Bsig.read() == '1' && CINsig.read() == '1')
            return !(Ysig.read() == '0' && COUTsig.read() == '1');

        if(Asig.read() == '1' && Bsig.read() == '0' && CINsig.read() == '0')
            return !(Ysig.read() == '1' && COUTsig.read() == '0');

        if(Asig.read() == '1' && Bsig.read() == '0' && CINsig.read() == '1')
            return !(Ysig.read() == '0' && COUTsig.read() == '1');

        if(Asig.read() == '1' && Bsig.read() == '1' && CINsig.read() == '0')
            return !(Ysig.read() == '0' && COUTsig.read() == '1');

        if(Asig.read() == '1' && Bsig.read() == '1' && CINsig.read() == '1')
            return !(Ysig.read() == '1' && COUTsig.read() == '1');  
              
    }           

    return 0;
    
}

