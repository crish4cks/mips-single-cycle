#include "mips.hpp"
#include "stimulus_mips.hpp"
#include "checker_mips.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);

    // Stimulus signals    
    sc_signal<sc_logic> RSTsig;
    sc_signal<sc_logic> PROGRAM_COUNTERsig[32];
    sc_signal<sc_logic> INSTRUCTIONsig[32];
    sc_signal<sc_logic> REGFILE_READ_REG_1sig[5]; 
    sc_signal<sc_logic> REGFILE_READ_REG_2sig[5]; 
    sc_signal<sc_logic> REGFILE_WRITE_REGsig[5];
    sc_signal<sc_logic> REGFILE_WRITE_DATAsig[32];
    sc_signal<sc_logic> REGFILE_READ_DATA_1sig[32];
    sc_signal<sc_logic> REGFILE_READ_DATA_2sig[32];
    sc_signal<sc_logic> MEM_DATA_ADDRESSsig[32];
    sc_signal<sc_logic> MEM_READ_DATAsig[32];
    sc_signal<sc_logic> MEM_WRITE_DATAsig[32];
    sc_signal<sc_logic> ALUSRCsig;
    sc_signal<sc_logic> MEMREADsig; 
    sc_signal<sc_logic> MEMWRITEsig;
    sc_signal<sc_logic> MEMTOREGsig; 
    sc_signal<sc_logic> REGDSTsig; 
    sc_signal<sc_logic> REGWRITEsig; 
    sc_signal<sc_logic> BRANCH_SELsig;
    sc_signal<sc_logic> JUMPsig;
    sc_signal<sc_logic> ALU_OPERATIONsig[4];
    sc_signal<sc_logic> Zsig, Vsig;
    
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    mips DUT("Mips");           
                   
    // Assign signals
    DUT.CLK(TestCLK);
    DUT.RST(RSTsig);
    DUT.ALUSRC(ALUSRCsig);
    DUT.MEMREAD(MEMREADsig); 
    DUT.MEMWRITE(MEMWRITEsig);
    DUT.MEMTOREG(MEMTOREGsig); 
    DUT.REGDST(REGDSTsig); 
    DUT.REGWRITE(REGWRITEsig); 
    DUT.BRANCH_SEL(BRANCH_SELsig);
    DUT.JUMP(JUMPsig);
    for(unsigned i = 0; i < 4; i++)
        DUT.ALU_OPERATION[i](ALU_OPERATIONsig[i]);
    DUT.Z(Zsig); 
    DUT.V(Vsig);    
    for(unsigned i = 0; i < 32; i++) {
        if(i < 5) {
            DUT.REGFILE_READ_REG_1[i](REGFILE_READ_REG_1sig[i]);  
            DUT.REGFILE_READ_REG_2[i](REGFILE_READ_REG_2sig[i]);
            DUT.REGFILE_WRITE_REG[i](REGFILE_WRITE_REGsig[i]);
        }      
    
        DUT.PROGRAM_COUNTER[i](PROGRAM_COUNTERsig[i]);
        DUT.INSTRUCTION[i](INSTRUCTIONsig[i]);
        DUT.REGFILE_WRITE_DATA[i](REGFILE_WRITE_DATAsig[i]);
        DUT.REGFILE_READ_DATA_1[i](REGFILE_READ_DATA_1sig[i]);
        DUT.REGFILE_READ_DATA_2[i](REGFILE_READ_DATA_2sig[i]);
        DUT.MEM_DATA_ADDRESS[i](MEM_DATA_ADDRESSsig[i]);
        DUT.MEM_READ_DATA[i](MEM_READ_DATAsig[i]);
        DUT.MEM_WRITE_DATA[i](MEM_WRITE_DATAsig[i]);
    }
    
      
    // Instantiate the stimulus generator
    stimulus_mips stim("StimGen");
    
    // Assign signals
    stim.CLK(TestCLK);
    stim.RST(RSTsig);
    
    
    // Instantiate the checker
    checker_mips chk("Checker");
    
    // Assign signals
    chk.CLK(TestCLK);
    chk.RST(RSTsig);
    chk.ALUSRC(ALUSRCsig);
    chk.MEMREAD(MEMREADsig); 
    chk.MEMWRITE(MEMWRITEsig);
    chk.MEMTOREG(MEMTOREGsig); 
    chk.REGDST(REGDSTsig); 
    chk.REGWRITE(REGWRITEsig); 
    chk.BRANCH_SEL(BRANCH_SELsig);
    chk.JUMP(JUMPsig);
    for(unsigned i = 0; i < 4; i++)
        chk.ALU_OPERATION[i](ALU_OPERATIONsig[i]);
    chk.Z(Zsig); 
    chk.V(Vsig);    
    for(unsigned i = 0; i < 32; i++) {
        if(i < 5) {
            chk.REGFILE_READ_REG_1[i](REGFILE_READ_REG_1sig[i]);  
            chk.REGFILE_READ_REG_2[i](REGFILE_READ_REG_2sig[i]);
            chk.REGFILE_WRITE_REG[i](REGFILE_WRITE_REGsig[i]);
        }      
    
        chk.PROGRAM_COUNTER[i](PROGRAM_COUNTERsig[i]);
        chk.INSTRUCTION[i](INSTRUCTIONsig[i]);
        chk.REGFILE_WRITE_DATA[i](REGFILE_WRITE_DATAsig[i]);
        chk.REGFILE_READ_DATA_1[i](REGFILE_READ_DATA_1sig[i]);
        chk.REGFILE_READ_DATA_2[i](REGFILE_READ_DATA_2sig[i]);
        chk.MEM_DATA_ADDRESS[i](MEM_DATA_ADDRESSsig[i]);
        chk.MEM_READ_DATA[i](MEM_READ_DATAsig[i]);
        chk.MEM_WRITE_DATA[i](MEM_WRITE_DATAsig[i]);
    }
        
    // Run simulation
    sc_start();                    
    
    return 0;
}
