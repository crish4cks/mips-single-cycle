#include <string>
#include "alu32.hpp"
#include "stimulus_alu32.hpp"
#include "checker_alu32.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);
    
    string str("");
    char ch[3];
    
    // Stimulus signals
    sc_signal<sc_logic> Asig[32], Bsig[32], A_INVsig, B_NEGsig, OPERATIONsig[2];
    sc_signal<sc_logic> RESsig[32], Zsig, Vsig, COUTsig;
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    alu32 DUT("Alu32");           
                   
    // Assign signals
    for(unsigned i = 0; i < 32; i++) {
        DUT.A[i](Asig[i]); 
	DUT.B[i](Bsig[i]);
	DUT.RES[i](RESsig[i]);
    }
    DUT.A_INV(A_INVsig);
    DUT.B_NEG(B_NEGsig);
    DUT.OPERATION[0](OPERATIONsig[0]);
    DUT.OPERATION[1](OPERATIONsig[1]);
    DUT.Z(Zsig);
    DUT.V(Vsig);
    DUT.COUT(COUTsig);
    
    
    // Check the number of arguments passed
    if(argc != 6) {
      
        // Instantiate the stimulus generator
        stimulus_alu32 stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            stim.A[i](Asig[i]); 
	    stim.B[i](Bsig[i]);
	}
	stim.A_INV(A_INVsig);
        stim.B_NEG(B_NEGsig);
        stim.OPERATION[0](OPERATIONsig[0]);
        stim.OPERATION[1](OPERATIONsig[1]);
      
	// Instantiate the checker
        checker_alu32 chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            chk.A[i](Asig[i]); 
	    chk.B[i](Bsig[i]);
	    chk.RES[i](RESsig[i]);
        }
        chk.A_INV(A_INVsig);
        chk.B_NEG(B_NEGsig);
        chk.OPERATION[0](OPERATIONsig[0]);
        chk.OPERATION[1](OPERATIONsig[1]);
        chk.Z(Zsig);
        chk.V(Vsig);
        chk.COUT(COUTsig);    
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("alu32");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Asig[i], "A(" + string(ch) + ")");
	    sc_trace(fp, Bsig[i], "B(" + string(ch) + ")");	    
	}
	sc_trace(fp, A_INVsig, "A_INV");
	sc_trace(fp, B_NEGsig, "B_NEG");
	sc_trace(fp, OPERATIONsig[1], "OPERATION(1)");
	sc_trace(fp, OPERATIONsig[0], "OPERATION(0)");
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, RESsig[i], "RES");
        sc_trace(fp, Zsig, "Z");
	sc_trace(fp, Vsig, "V");
	sc_trace(fp, COUTsig, "COUT");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++) {
            Asig[i].write(sc_logic(argv[1][j]));
	    Bsig[i].write(sc_logic(argv[2][j]));
	}
	
	A_INVsig.write(sc_logic(argv[3][0]));
	B_NEGsig.write(sc_logic(argv[4][0]));
	
	OPERATIONsig[0].write(sc_logic(argv[5][1]));
	OPERATIONsig[1].write(sc_logic(argv[5][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> asig = argv[1];
	sc_lv<32> bsig = argv[2];
	sc_lv<2> operation = argv[5];	
	
        if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000000011") && A_INVsig.read() == '0' && B_NEGsig.read() == '0' && operation == sc_lv<2>("10")) {
	    sc_lv<32> res = "00000000000000000000000000001101";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '0')
	        return 1;
            return 0;	    
	}

	if(asig == sc_lv<32>("01111111111111111111111111111111") && bsig == sc_lv<32>("01111111111111111111111111111111") && A_INVsig.read() == '0' && B_NEGsig.read() == '0' && operation == sc_lv<2>("10")) {
	    sc_lv<32> res = "11111111111111111111111111111110";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '1' || COUTsig.read() != '0')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000000011") && A_INVsig.read() == '0' && B_NEGsig.read() == '1' && operation == sc_lv<2>("10")) {
	    sc_lv<32> res = "00000000000000000000000000000111";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '1')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000001011") && A_INVsig.read() == '0' && B_NEGsig.read() == '1' && operation == sc_lv<2>("10")) {
	    sc_lv<32> res = "11111111111111111111111111111111";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '0')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000000011") && A_INVsig.read() == '0' && B_NEGsig.read() == '1' && operation == sc_lv<2>("11")) {
	    sc_lv<32> res = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '1' || Vsig.read() != '0' || COUTsig.read() != '1')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000001011") && A_INVsig.read() == '0' && B_NEGsig.read() == '1' && operation == sc_lv<2>("11")) {
	    sc_lv<32> res = "00000000000000000000000000000001";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '0')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00010000000001000000000000001010") && bsig == sc_lv<32>("00010000100000000000000000001011") && A_INVsig.read() == '0' && B_NEGsig.read() == '0' && operation == sc_lv<2>("00")) {
	    sc_lv<32> res = "00010000000000000000000000001010";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '0')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000001011") && A_INVsig.read() == '0' && B_NEGsig.read() == '0' && operation == sc_lv<2>("01")) {
	    sc_lv<32> res = "00000000000000000000000000001011";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '0')
	        return 1;
            return 0;
	}
	
	if(asig == sc_lv<32>("00000000000000000000000000001010") && bsig == sc_lv<32>("00000000000000000000000000001011") && A_INVsig.read() == '1' && B_NEGsig.read() == '1' && operation == sc_lv<2>("00")) {
	    sc_lv<32> res = "11111111111111111111111111110100";
            for(unsigned i = 0; i < 32; i++)
	        if(RESsig[i].read() != res[i])
		    return 1;  
	    if(Zsig.read() != '0' || Vsig.read() != '0' || COUTsig.read() != '1')
	        return 1;
            return 0;
	}

    }           
    
    return 0;
    
}
