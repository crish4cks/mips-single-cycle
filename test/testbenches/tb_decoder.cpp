#include <string>
#include "decoder.hpp"
#include "stimulus_decoder.hpp"
#include "checker_decoder.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> Xsig[5];
    sc_signal<sc_logic> Ysig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    decoder<5,32> DUT("Decoder_5x32");           
                   
    // Assign signals
    for(unsigned i = 0; i < 5; i++)
        DUT.X[i](Xsig[i]);
    for(unsigned i = 0; i < 32; i++)
	DUT.Y[i](Ysig[i]);
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_decoder stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 5; i++)
            stim.X[i](Xsig[i]); 	    
      	
	// Instantiate the checker
        checker_decoder chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 5; i++)
            chk.X[i](Xsig[i]);
        for(unsigned i = 0; i < 32; i++)
	    chk.Y[i](Ysig[i]);        
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("Decoder_5x32");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file	
	for(int i = 4; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Xsig[i], "X(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, Ysig[i], "Y");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 4, j = 0; i >= 0, j < 5; i--, j++)
            Xsig[i].write(sc_logic(argv[1][j]));	
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> xsig = argv[1];	
	
        if(xsig == sc_lv<32>("00011")) {
	    sc_lv<32> y = "00000000000000000000000000001000";
            for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != y[i])
		    return 1;  
            return 0;	    
	}
	
	if(xsig == sc_lv<32>("01010")) {
	    sc_lv<32> y = "00000000000000000000010000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != y[i])
		    return 1;  
            return 0;	    
	}
	
	if(xsig == sc_lv<32>("11111")) {
	    sc_lv<32> y = "10000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != y[i])
		    return 1;  
            return 0;	    
	}

    }           
    
    return 0;
    
}
