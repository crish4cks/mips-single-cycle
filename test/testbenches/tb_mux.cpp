#include <string>
#include "mux.hpp"
#include "stimulus_mux.hpp"
#include "checker_mux.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  
    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);
    
    string str("");
    char ch[3];
    
    // Stimulus signals
    sc_signal<sc_logic> Asig[128];
    sc_signal<sc_logic> SELsig[2];
    sc_signal<sc_logic> Ysig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    mux<2,32,128> DUT("Mux");           
                   
    // Assign signals
    for(unsigned i = 0; i < 128; i++)
        DUT.A[i](Asig[i]);    
    DUT.SEL[0](SELsig[0]);
    DUT.SEL[1](SELsig[1]);
    for(unsigned i = 0; i < 32; i++)
        DUT.Y[i](Ysig[i]);
    
    
    // Check the number of arguments passed
    if(argc != 3) {
      
        // Instantiate the stimulus generator
        stimulus_mux stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 128; i++)
            stim.A[i](Asig[i]);
	stim.SEL[0](SELsig[0]);
	stim.SEL[1](SELsig[1]);
      
	// Instantiate the checker
        checker_mux chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 128; i++)
            chk.A[i](Asig[i]);   
	chk.SEL[0](SELsig[0]);
	chk.SEL[1](SELsig[1]);
	for(unsigned i = 0; i < 32; i++)
            chk.Y[i](Ysig[i]);
    
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("mux");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	for(int i = 127; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Asig[i], "A(" + string(ch) + ")");
	}	
	sc_trace(fp, SELsig[1], "SEL(1)");
	sc_trace(fp, SELsig[0], "SEL(0)");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, Ysig[i], "Y(" + string(ch) + ")");
	}
        //sc_trace(fp, Ysig, "Y");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 127, j = 0; i >= 0, j < 128; i--, j++)
            Asig[i].write(sc_logic(argv[1][j]));
	
	SELsig[0].write(sc_logic(argv[2][1]));
	SELsig[1].write(sc_logic(argv[2][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<128> asig = argv[1];
	sc_lv<2> selsig = argv[2];	
	
        if(asig == sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("01")) {
	    sc_lv<32> res = "00000000000000000000000000001100";
            for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}

	if(asig == sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("00")) {
	    sc_lv<32> res = "00000000000000000000000000001111";
	    for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}
	    
	if(asig == sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("11")) {
	    sc_lv<32> res = "00000000000000000000000000001010";
	    for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}
	    
	if(asig == sc_lv<128>("00000000000000000000000000001000000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("11")) {
	    sc_lv<32> res = "00000000000000000000000000001000";
	    for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}
	    
	if(asig == sc_lv<128>("00000000000000000000000000001001000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("11")) {
	    sc_lv<32> res = "00000000000000000000000000001001";
	    for(unsigned i = 0; i < 32; i++)
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}
	    
	if(asig == sc_lv<128>("00000000000000000000000000001000000000000000000000000000000001110000000000000000000000000000110000000000000000000000000000001111") && selsig == sc_lv<2>("10")) {
	    sc_lv<32> res = "00000000000000000000000000000111";
	    for(unsigned i = 0; i < 32; i++) 	        
	        if(Ysig[i].read() != res[i])
		    return 1;  
            return 0;
	}

    }           

    return 0;
    
}

