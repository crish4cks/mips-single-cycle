#include "not_gate.hpp"
#include "stimulus_not_gate.hpp"
#include "checker_not_gate.hpp"

int sc_main(int argc, char* argv[]) {
    
    // Stimulus signals
    sc_signal<sc_logic> Asig;
    sc_signal<sc_logic> Ysig;
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    not_gate DUT("NotGate");           
                   
    // Assign signals
    DUT.A(Asig);                             
    DUT.Y(Ysig);
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_not_gate stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
        stim.A(Asig);
      
	// Instantiate the checker
        checker_not_gate chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
        chk.A(Asig);                             
        chk.Y(Ysig);
    
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("not_gate");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
        sc_trace(fp, Asig, "A");                  
        sc_trace(fp, Ysig, "Y");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        Asig.write(sc_logic(argv[1][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
        if(Asig.read() == '0')
            return !(Ysig.read() == '1');

	if(Asig.read() == '1')
            return !(Ysig.read() == '0');
	
	
    }           

    return 0;
    
}

