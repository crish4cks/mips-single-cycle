#include <string>
#include "shifter.hpp"
#include "stimulus_shifter.hpp"
#include "checker_shifter.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
    
    string str("");
    char ch[3];
    
    // Stimulus signals
    sc_signal<sc_logic> INPUTsig[32], DIRECTIONsig;
    sc_signal<sc_logic> OUTPUTsig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);
    
    
    // Instantiate the Device Under Test (default constructor)
    shifter<2> DUT("ShiftLeft2");           
                   
    // Assign signals
    for(unsigned i = 0; i < 32; i++) {
        DUT.INPUT[i](INPUTsig[i]); 	
	DUT.OUTPUT[i](OUTPUTsig[i]);
    }
    DUT.DIRECTION(DIRECTIONsig);
    
    
    // Check the number of arguments passed
    if(argc != 3) {
      
        // Instantiate the stimulus generator
        stimulus_shifter stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++)
            stim.INPUT[i](INPUTsig[i]); 	    
	stim.DIRECTION(DIRECTIONsig);
      	
	// Instantiate the checker
        checker_shifter chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 32; i++) {
            chk.INPUT[i](INPUTsig[i]); 
	    chk.OUTPUT[i](OUTPUTsig[i]);
        }
        chk.DIRECTION(DIRECTIONsig);    
	
        // Only for graphic simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("ShiftLeft2");
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	for(int i = 31; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, INPUTsig[i], "INPUT(" + string(ch) + ")");	    
	}
	sc_trace(fp, DIRECTIONsig, "DIRECTON");
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, OUTPUTsig[i], "OUTPUT");
        
        // Run simulation	
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++)
            INPUTsig[i].write(sc_logic(argv[1][j]));
	
	DIRECTIONsig.write(sc_logic(argv[2][0]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<32> inputsig = argv[1];	
	
        if(inputsig == sc_lv<32>("00000000000000000000000000001010") && DIRECTIONsig.read() == '0') {
	    sc_lv<32> output = "00000000000000000000000000101000";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}
	
	if(inputsig == sc_lv<32>("00000000000000000000000000001010") && DIRECTIONsig.read() == '1') {
	    sc_lv<32> output = "00000000000000000000000000000010";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}
	
	if(inputsig == sc_lv<32>("10000000000000000000000000101011") && DIRECTIONsig.read() == '0') {
	    sc_lv<32> output = "00000000000000000000000010101100";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}
	
	if(inputsig == sc_lv<32>("00000000000000000001000000101011") && DIRECTIONsig.read() == '1') {
	    sc_lv<32> output = "00000000000000000000010000001010";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}

    }           
    
    return 0;
    
}
