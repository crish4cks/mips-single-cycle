#include <string>
#include "sign_extender.hpp"
#include "stimulus_sign_extender.hpp"
#include "checker_sign_extender.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> INPUTsig[16], OUTPUTsig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    sign_extender<16,32> DUT("SignExtender_16_32");           
                   
    // Assign signals
    for(unsigned i = 0; i < 16; i++)
        DUT.INPUT[i](INPUTsig[i]); 	
    for(unsigned i = 0; i < 32; i++)
        DUT.OUTPUT[i](OUTPUTsig[i]); 	
    
    
    // Check the number of arguments passed
    if(argc != 2) {
      
        // Instantiate the stimulus generator
        stimulus_sign_extender stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	for(unsigned i = 0; i < 16; i++)
            stim.INPUT[i](INPUTsig[i]); 	    
      	
	// Instantiate the checker
        checker_sign_extender chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	for(unsigned i = 0; i < 16; i++)
            chk.INPUT[i](INPUTsig[i]); 
        for(unsigned i = 0; i < 32; i++)
            chk.OUTPUT[i](OUTPUTsig[i]);
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("sign_extender_16_32");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");	
	for(int i = 15; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, INPUTsig[i], "INPUT(" + string(ch) + ")");	    
	}
	for(int i = 31; i >= 0; i--)
	    sc_trace(fp, OUTPUTsig[i], "OUTPUT");

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
      
        // Take the inputs from command line
        for(int i = 15, j = 0; i >= 0, j < 16; i--, j++)
            INPUTsig[i].write(sc_logic(argv[1][j]));
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<16> input = argv[1];	
	
        if(input == sc_lv<16>("0000000000000010")) {
	    sc_lv<32> output = "00000000000000000000000000000010";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}
	
	if(input == sc_lv<16>("0000000000000101")) {
	    sc_lv<32> output = "00000000000000000000000000000101";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}
	
	if(input == sc_lv<16>("1000000000000001")) {
	    sc_lv<32> output = "11111111111111111000000000000001";
            for(unsigned i = 0; i < 32; i++)
	        if(OUTPUTsig[i].read() != output[i])
		    return 1;  
            return 0;	    
	}

    }           
    
    return 0;
    
}
