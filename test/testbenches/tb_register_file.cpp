#include <string>
#include "register_file.hpp"
#include "stimulus_register_file.hpp"
#include "checker_register_file.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

    // Suppress zero-time warnings related to numeric conversions
    sc_report_handler::set_actions(SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_DO_NOTHING);

    string str("");
    char ch[3];

    // Stimulus signals
    sc_signal<sc_logic> RSTsig, REGWRITEsig, READ_REGISTER_1sig[5], READ_REGISTER_2sig[5], WRITE_REGISTERsig[5], WRITE_DATAsig[32];
    sc_signal<sc_logic> READ_DATA_1sig[32], READ_DATA_2sig[32];
    sc_clock TestCLK("TestClock", 100, SC_NS, 0.5, 0, SC_NS);


    // Instantiate the Device Under Test (default constructor)
    register_file DUT("RegisterFile");           
                   
    // Assign signals
    DUT.CLK(TestCLK);
    DUT.RST(RSTsig);
    DUT.REGWRITE(REGWRITEsig);
    for(unsigned i = 0; i < 5; i++) {
        DUT.READ_REGISTER_1[i](READ_REGISTER_1sig[i]);  
	DUT.READ_REGISTER_2[i](READ_REGISTER_2sig[i]);
	DUT.WRITE_REGISTER[i](WRITE_REGISTERsig[i]);
    }
    for(unsigned i = 0; i < 32; i++) {
        DUT.WRITE_DATA[i](WRITE_DATAsig[i]); 	
	DUT.READ_DATA_1[i](READ_DATA_1sig[i]);
	DUT.READ_DATA_2[i](READ_DATA_2sig[i]);
    }
    
    
    // Check the number of arguments passed
    if(argc != 7) {
      
        // Instantiate the stimulus generator
        stimulus_register_file stim("StimGen");
    
        // Assign signals
        stim.CLK(TestCLK);
	stim.RST(RSTsig);
        stim.REGWRITE(REGWRITEsig);
	for(unsigned i = 0; i < 5; i++) {
	    stim.READ_REGISTER_1[i](READ_REGISTER_1sig[i]);  
	    stim.READ_REGISTER_2[i](READ_REGISTER_2sig[i]);
	    stim.WRITE_REGISTER[i](WRITE_REGISTERsig[i]);
	  
	}
	for(unsigned i = 0; i < 32; i++)
            stim.WRITE_DATA[i](WRITE_DATAsig[i]); 	    	
      	
	// Instantiate the checker
        checker_register_file chk("Checker");
    
        // Assign signals
        chk.CLK(TestCLK);
	chk.RST(RSTsig);
        chk.REGWRITE(REGWRITEsig);
        for(unsigned i = 0; i < 5; i++) {
            chk.READ_REGISTER_1[i](READ_REGISTER_1sig[i]);  
	    chk.READ_REGISTER_2[i](READ_REGISTER_2sig[i]);
	    chk.WRITE_REGISTER[i](WRITE_REGISTERsig[i]);
        }
        for(unsigned i = 0; i < 32; i++) {
            chk.WRITE_DATA[i](WRITE_DATAsig[i]); 	
     	    chk.READ_DATA_1[i](READ_DATA_1sig[i]);
	    chk.READ_DATA_2[i](READ_DATA_2sig[i]);
        }
	
        // Only for graphical simulation
        sc_trace_file *fp;          
    
        // Create (and open in write mode) a .vcd file
        fp = sc_create_vcd_trace_file("register_file");  
    
        // Set tracing resolution
        fp->set_time_unit(1, SC_NS);          
    
        // Add signals to trace file
	sc_trace(fp, TestCLK, "CLK");
	sc_trace(fp, RSTsig, "RST");
	sc_trace(fp, REGWRITEsig, "REGWRITE");
	for(int i = 5; i >= 0; i--) {
            sprintf(ch, "%d", i);
	    sc_trace(fp, READ_REGISTER_1sig[i], "READ_REGISTER_1(" + string(ch) + ")");
	    sc_trace(fp, READ_REGISTER_2sig[i], "READ_REGISTER_2(" + string(ch) + ")");
	    sc_trace(fp, WRITE_REGISTERsig[i], "WRITE_REGISTER(" + string(ch) + ")");
	}
	for(int i = 31; i >= 0; i--) {
	    sprintf(ch, "%d", i);
	    sc_trace(fp, WRITE_DATAsig[i], "WRITE_DATA(" + string(ch) + ")");
	    sc_trace(fp, READ_DATA_1sig[i], "READ_DATA_1");
	    sc_trace(fp, READ_DATA_1sig[i], "READ_DATA_2");
	}

        // Run simulation
        sc_start();
	//sc_start(100, SC_NS); 
    
        // Close the .vcd file
        sc_close_vcd_trace_file(fp);
    
        // Launch gtkwave to see the output waveforms (you need to have it installed on your OS)
        //system("gtkwave wave.vcd");        

    }  
    else {
        
        // Take the inputs from command line
        RSTsig.write(sc_logic(argv[1][0]));
	REGWRITEsig.write(sc_logic(argv[2][0]));
        
        for(int i = 4, j = 0; i >= 0, j < 5; i--, j++) {
            READ_REGISTER_1sig[i].write(sc_logic(argv[3][j]));
	    READ_REGISTER_2sig[i].write(sc_logic(argv[4][j]));
	    WRITE_REGISTERsig[i].write(sc_logic(argv[5][j]));
        }
        
        for(int i = 31, j = 0; i >= 0, j < 32; i--, j++)
            WRITE_DATAsig[i].write(sc_logic(argv[6][j]));
	
	
	
	// Perform tests with cmake
	
	sc_start(1, SC_NS);
	
	sc_lv<5> read_register_1 = argv[3];	
	sc_lv<5> read_register_2 = argv[4];
	sc_lv<5> write_register = argv[5];
	sc_lv<32> write_data = argv[6];
	
        if(RSTsig.read() == '0' && REGWRITEsig.read() == '1' && read_register_1 == sc_lv<5>("00001") && read_register_2 == sc_lv<5>("00010") && write_register == sc_lv<5>("00101") && write_data == sc_lv<32>("00000000000000000000000000001010")) {
	    sc_lv<32> read_data = "00000000000000000000000000000000";
            for(unsigned i = 0; i < 32; i++) {
	        if(READ_DATA_1sig[i].read() != read_data[i] || READ_DATA_2sig[i].read() != read_data[i])
		    return 1;  
	    }
            return 0;	    
	}
	
    }           
    
    return 0;
    
}
