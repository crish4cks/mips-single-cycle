#ifndef CHECKER_SHIFTER_HPP
#define CHECKER_SHIFTER_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_shifter) {

    sc_in<sc_logic> INPUT[32], OUTPUT[32], DIRECTION;
    sc_in<bool> CLK;

    void check() {

        sc_lv<32> input, output;

        cout << setw(7) << "Time";  
	cout << setw(7) << "INPUT";
	cout << setw(14) << "DIRECTION";
	cout << setw(8) << "OUTPUT" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    for(unsigned i = 0; i < 32; i++)
	        input[i] = INPUT[i].read();
	    cout << setw(10) << input.to_string(SC_HEX_US, false);
	    cout << setw(3) << DIRECTION.read();
	    for(unsigned i = 0; i < 32; i++)
	        output[i] = OUTPUT[i].read();
	    cout << setw(18) << output.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_shifter) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
