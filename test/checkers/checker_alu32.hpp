#ifndef CHECKER_ALU32_HPP
#define CHECKER_ALU32_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_alu32) {
    
    sc_in<sc_logic> A[32], B[32], A_INV, B_NEG, RES[32], Z, V, COUT;
    sc_in<sc_logic> OPERATION[2];
    sc_in<bool> CLK;
    
    void check() {
      
        sc_lv<32> a, b, res;
	sc_lv<2> operation;
      
        cout << setw(7) << "Time";  
	cout << setw(3) << "A";
	cout << setw(10) << "B";
	cout << setw(14) << "A_INV";
	cout << setw(7) << "B_NEG";
	cout << setw(11) << "OPERATION";
	cout << setw(5) << "RES";
	cout << setw(8) << "Z";
	cout << setw(3) << "V";
	cout << setw(6) << "COUT" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    for(unsigned i = 0; i < 32; i++)
	        a[i] = A[i].read();
	    cout << setw(10) << a.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 32; i++)
	        b[i] = B[i].read();
	    cout << setw(10) << b.to_string(SC_HEX_US, false);
	    cout << setw(3) << A_INV.read();
	    cout << setw(7) << B_NEG.read();
	    operation[0] = OPERATION[0].read();
	    operation[1] = OPERATION[1].read();
	    cout << hex << setw(7) << operation;
	    for(unsigned i = 0; i < 32; i++)
	        res[i] = RES[i].read();
	    cout << setw(18) << res.to_string(SC_HEX_US, false);
	    cout << setw(3) << Z.read();
	    cout << setw(3) << V.read();
	    cout << setw(3) << COUT.read() << endl;	    
        }
    }
    
    SC_CTOR(checker_alu32) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
  
};

#endif