#ifndef CHECKER_DATA_MEMORY_HPP
#define CHECKER_DATA_MEMORY_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_data_memory) {

    sc_in<sc_logic> RST, ADDRESS[32], WRITE_DATA[32], READ_DATA[32], MEMREAD, MEMWRITE;
    sc_in<bool> CLK;

    void check() {

        sc_lv<32> address, write_data, read_data;

        cout << setw(7) << "Time";  
	cout << setw(5) << "CLK";
	cout << setw(5) << "RST";
	cout << setw(9) << "ADDRESS";
        cout << setw(13) << "WRITE_DATA";
	cout << setw(11) << "READ_DATA";
	cout << setw(9) << "MEMREAD";
	cout << setw(10) << "MEMWRITE" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();
            cout << setw(3) << CLK.read();
            cout << setw(5) << RST.read();            
	    for(unsigned i = 0; i < 32; i++) {
	        address[i] = ADDRESS[i].read();
		write_data[i] = WRITE_DATA[i].read();
		read_data[i] = READ_DATA[i].read();
	    }
	    cout << setw(12) << address.to_string(SC_HEX_US, false);
	    cout << setw(10) << write_data.to_string(SC_HEX_US, false);
	    cout << setw(12) << read_data.to_string(SC_HEX_US, false);
	    cout << setw(4) << MEMREAD.read();
	    cout << setw(9) << MEMWRITE.read() << endl;
        }
    }

    SC_CTOR(checker_data_memory) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
