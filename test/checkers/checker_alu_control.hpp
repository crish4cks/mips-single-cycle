#ifndef CHECKER_ALU_CONTROL_HPP
#define CHECKER_ALU_CONTROL_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_alu_control) {

    sc_in<sc_logic> ALUOP[2], FUNCTION_CODE[6], OPERATION[4];
    sc_in<bool> CLK;

    void check() {

        sc_lv<2> aluop;
        sc_lv<6> function_code;	
        sc_lv<4> operation;
	
        cout << setw(7) << "Time";  
        cout << setw(7) << "ALUOP";
	cout << setw(15) << "FUNCTION_CODE";
        cout << setw(11) << "OPERATION" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();	    
	    aluop[1] = ALUOP[1].read();
	    aluop[0] = ALUOP[0].read();
	    cout << setw(3) << aluop.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 6; i++)
	        function_code[i] = FUNCTION_CODE[i].read();
	    cout << setw(8) << function_code.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 4; i++)
	        operation[i] = OPERATION[i].read();
	    cout << setw(14) << operation.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_alu_control) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
