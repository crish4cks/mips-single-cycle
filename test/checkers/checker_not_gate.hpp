#ifndef CHECKER_NOT_GATE_HPP
#define CHECKER_NOT_GATE_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_not_gate) {
    
    sc_in<sc_logic> A;
    sc_in<sc_logic> Y;
    sc_in<bool> CLK;
    
    void check() {
      
        cout << setw(7) << "Time";  
	cout << setw(5) << "A";
	cout << setw(5) << "Y" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    cout << setw(5) << A.read();
	    cout << setw(5) << Y.read() << endl;
        }
    }
    
    SC_CTOR(checker_not_gate) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
  
};

#endif
