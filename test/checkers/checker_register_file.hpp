#ifndef CHECKER_REGISTER_FILE_HPP
#define CHECKER_REGISTER_FILE_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_register_file) {

    sc_in<sc_logic> RST, REGWRITE, READ_REGISTER_1[5], READ_REGISTER_2[5], WRITE_REGISTER[5], WRITE_DATA[32], READ_DATA_1[32], READ_DATA_2[32];
    sc_in<bool> CLK;

    void check() {
 
        sc_lv<5> read_register_1, read_register_2, write_register;
        sc_lv<32> write_data, read_data_1, read_data_2;

        cout << setw(7) << "Time";  
	cout << setw(5) << "CLK";
	cout << setw(5) << "RST";
	cout << setw(10) << "REGWRITE";
	cout << setw(17) << "READ_REGISTER_1";
	cout << setw(17) << "READ_REGISTER_2";
	cout << setw(16) << "WRITE_REGISTER";
	cout << setw(12) << "WRITE_DATA";
	cout << setw(13) << "READ_DATA_1";
	cout << setw(13) << "READ_DATA_2" << endl;

	while(true) {
	    wait(50, SC_NS); // wait one half of clock cycle
	    cout << setw(7) << sc_time_stamp();
            cout << setw(3) << CLK.read();
            cout << setw(5) << RST.read();
            cout << setw(5) << REGWRITE.read();
	    for(unsigned i = 0; i < 5; i++) {
	        read_register_1[i] = READ_REGISTER_1[i].read();
		read_register_2[i] = READ_REGISTER_2[i].read();
		write_register[i] = WRITE_REGISTER[i].read();
	    }
	    cout << setw(11) << read_register_1.to_string(SC_HEX_US, false);
	    cout << setw(17) << read_register_2.to_string(SC_HEX_US, false);
	    cout << setw(17) << write_register.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 32; i++) {
	        write_data[i] = WRITE_DATA[i].read();
		read_data_1[i] = READ_DATA_1[i].read();
		read_data_2[i] = READ_DATA_2[i].read();
	    }
	    cout << setw(22) << write_data.to_string(SC_HEX_US, false);
	    cout << setw(12) << read_data_1.to_string(SC_HEX_US, false);
	    cout << setw(13) << read_data_2.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_register_file) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
