#ifndef CHECKER_FULL_ADDER_HPP
#define CHECKER_FULL_ADDER_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_full_adder) {
    
    sc_in<sc_logic> A, B, CIN, Y, COUT;
    sc_in<bool> CLK;
    
    void check() {
      
        cout << setw(7) << "Time";  
	cout << setw(5) << "A";
	cout << setw(5) << "B";
	cout << setw(7) << "CIN";
	cout << setw(3) << "Y";
	cout << setw(8) << "COUT" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    cout << setw(5) << A.read();
	    cout << setw(5) << B.read();
	    cout << setw(5) << CIN.read();
	    cout << setw(5) << Y.read();
	    cout << setw(5) << COUT.read() << endl;	    
        }
    }
    
    SC_CTOR(checker_full_adder) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
    
  
};

#endif
