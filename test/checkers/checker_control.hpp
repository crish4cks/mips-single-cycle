#ifndef CHECKER_CONTROL_HPP
#define CHECKER_CONTROL_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_control) {

    sc_in<sc_logic> OPCODE[6], ALUOP[2], ALUSRC, MEMREAD, MEMWRITE, MEMTOREG, REGDST, REGWRITE, BRANCH, JUMP;
    sc_in<bool> CLK;

    void check() {

        sc_lv<6> opcode;
	sc_lv<2> aluop;

        cout << setw(7) << "Time";  
	cout << setw(8) << "OPCODE";
	cout << setw(7) << "ALUOP";
	cout << setw(8) << "ALUSRC";
        cout << setw(9) << "MEMREAD";
	cout << setw(10) << "MEMWRITE";
	cout << setw(10) << "MEMTOREG";
	cout << setw(8) << "REGDST";
	cout << setw(10) << "REGWRITE";
	cout << setw(8) << "BRANCH";
        cout << setw(6) << "JUMP" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();            
	    for(unsigned i = 0; i < 6; i++)
	        opcode[i] = OPCODE[i].read();
	    cout << setw(8) << opcode;
	    aluop[1] = ALUOP[1].read();
	    aluop[0] = ALUOP[0].read();
	    cout << setw(4) << aluop;
	    cout << setw(6) << ALUSRC.read();
            cout << setw(8) << MEMREAD.read();
            cout << setw(9) << MEMWRITE.read();
	    cout << setw(10) << MEMTOREG.read();
	    cout << setw(10) << REGDST.read();
	    cout << setw(8) << REGWRITE.read();
	    cout << setw(10) << BRANCH.read();
	    cout << setw(8) << JUMP.read() << endl;
        }
    }

    SC_CTOR(checker_control) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
