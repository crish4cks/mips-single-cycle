#ifndef CHECKER_DECODER_HPP
#define CHECKER_DECODER_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_decoder) {

    sc_in<sc_logic> X[5], Y[32];
    sc_in<bool> CLK;

    void check() {

        sc_lv<5> x;
        sc_lv<32> y;

        cout << setw(7) << "Time";  	
        cout << setw(3) << "X";
        cout << setw(4) << "Y" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();
	    for(unsigned i = 0; i < 5; i++)
	        x[i] = X[i].read();
	    cout << setw(4) << x.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 32; i++)
	        y[i] = Y[i].read();
	    cout << setw(10) << y.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_decoder) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
