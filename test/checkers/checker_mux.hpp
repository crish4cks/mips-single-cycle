#ifndef CHECKER_MUX_HPP
#define CHECKER_MUX_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_mux) {
    
    sc_in<sc_logic> A[128];
    sc_in<sc_logic> Y[32];
    sc_in<sc_logic> SEL[2];
    sc_in<bool> CLK;
    
    void check() {
        sc_lv<128> a;
        sc_lv<32> y;
        sc_lv<2> sel;
      
        cout << setw(7) << "Time"; 
	cout << setw(3) << "A";
	cout << setw(36) << "SEL";
	cout << setw(3) << "Y" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    cout << hex << setw(34);
	    for(unsigned i = 0; i < 128; i++)
	        a[i] = A[i].read();    
	    cout << a.to_string(SC_HEX_US, false);
	    sel[0] = SEL[0].read();
	    sel[1] = SEL[1].read();
	    cout << hex << setw(3) << sel;
	    cout << hex << setw(12); 
	    for(unsigned i = 0; i < 32; i++)
	        y[i] = Y[i].read();
	    cout << y.to_string(SC_HEX_US, false) << endl; // prints bits from MSB to LSB	    
        }
    }
    
    SC_CTOR(checker_mux) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
    
  
};

#endif
