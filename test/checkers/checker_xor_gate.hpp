#ifndef CHECKER_XOR_GATE_HPP
#define CHECKER_XOR_GATE_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_xor_gate) {
    
    sc_in<sc_logic> A[3];
    sc_in<sc_logic> Y;
    sc_in<bool> CLK;
    
    void check() {
      
        cout << setw(7) << "Time";  
	cout << setw(5) << "A";
	cout << setw(7) << "Y" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    cout << setw(5);
	    for(int i = 2; i >= 0; i--)
	        cout << A[i].read();
	    cout << setw(5) << Y.read() << endl;
        }
    }
    
    SC_CTOR(checker_xor_gate) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
    
  
};

#endif
