#ifndef CHECKER_MIPS_HPP
#define CHECKER_MIPS_HPP

#include <iostream>
#include <fstream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_mips) {
    
    sc_in<bool> CLK;
    sc_in<sc_logic> RST; 
    sc_in<sc_logic> INSTRUCTION[32];
    sc_in<sc_logic> PROGRAM_COUNTER[32];
    sc_in<sc_logic> REGFILE_READ_REG_1[5]; 
    sc_in<sc_logic> REGFILE_READ_REG_2[5];
    sc_in<sc_logic> REGFILE_WRITE_REG[5];
    sc_in<sc_logic> REGFILE_WRITE_DATA[32];
    sc_in<sc_logic> REGFILE_READ_DATA_1[32];
    sc_in<sc_logic> REGFILE_READ_DATA_2[32];
    sc_in<sc_logic> MEM_DATA_ADDRESS[32];
    sc_in<sc_logic> MEM_READ_DATA[32];
    sc_in<sc_logic> MEM_WRITE_DATA[32];
    sc_in<sc_logic> ALUSRC;
    sc_in<sc_logic> MEMREAD; 
    sc_in<sc_logic> MEMWRITE;
    sc_in<sc_logic> MEMTOREG; 
    sc_in<sc_logic> REGDST; 
    sc_in<sc_logic> REGWRITE; 
    sc_in<sc_logic> BRANCH_SEL;
    sc_in<sc_logic> JUMP;
    sc_in<sc_logic> ALU_OPERATION[4];
    sc_in<sc_logic> Z, V;
    
    ofstream logger;

    void check() {

        sc_lv<4> alu_operation;
        sc_lv<5> regfile_read_reg_1, regfile_read_reg_2, regfile_write_reg;
        sc_lv<32> instruction; 
        sc_lv<32> program_counter;
        sc_lv<32> mem_data_address;
        sc_lv<32> mem_read_data; 
        sc_lv<32> mem_write_data; 
        sc_lv<32> regfile_write_data; 
        sc_lv<32> regfile_read_data_1; 
        sc_lv<32> regfile_read_data_2;

        logger << setw(7) << "Time";  
        logger << setw(5) << "CLK";
        logger << setw(5) << "RST";
        logger << setw(13) << "INSTRUCTION";
        logger << setw(17) << "PROGRAM_COUNTER";
        logger << setw(20) << "REGFILE_READ_REG_1";
        logger << setw(20) << "REGFILE_READ_REG_2";
        logger << setw(19) << "REGFILE_WRITE_REG";
        logger << setw(20) << "REGFILE_WRITE_DATA";
        logger << setw(21) << "REGFILE_READ_DATA_1";
        logger << setw(21) << "REGFILE_READ_DATA_2";
        logger << setw(18) << "MEM_DATA_ADDRESS";
        logger << setw(15) << "MEM_READ_DATA";
        logger << setw(16) << "MEM_WRITE_DATA";
        logger << setw(8) << "ALUSRC";
        logger << setw(9) << "MEMREAD"; 
        logger << setw(10) << "MEMWRITE";
        logger << setw(10) << "MEMTOREG"; 
        logger << setw(8) << "REGDST"; 
        logger << setw(10) << "REGWRITE"; 
        logger << setw(12) << "BRANCH_SEL";
        logger << setw(6) << "JUMP";
        logger << setw(15) << "ALU_OPERATION";
        logger << setw(3) << 'Z';
        logger << setw(3) << 'V' << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    
	    for(unsigned i = 0; i < 5; i++) {
	        regfile_read_reg_1[i] = REGFILE_READ_REG_1[i].read();
		regfile_read_reg_2[i] = REGFILE_READ_REG_2[i].read();
		regfile_write_reg[i] = REGFILE_WRITE_REG[i].read();
		if(i < 4)
		    alu_operation[i] = ALU_OPERATION[i].read();
	    }
	   
	    for(unsigned i = 0; i < 32; i++) {
	        instruction[i] = INSTRUCTION[i].read();
	        program_counter[i] = PROGRAM_COUNTER[i].read();
	        regfile_write_data[i] = REGFILE_WRITE_DATA[i].read();
                regfile_read_data_1[i] = REGFILE_READ_DATA_1[i].read();
                regfile_read_data_2[i] = REGFILE_READ_DATA_2[i].read();
                mem_data_address[i] = MEM_DATA_ADDRESS[i].read();
                mem_read_data[i] = MEM_READ_DATA[i].read();
                mem_write_data[i] = MEM_WRITE_DATA[i].read();
	    }
	    
	    logger << setw(7) << sc_time_stamp();
            logger << setw(3) << CLK.read();
            logger << setw(5) << RST.read();
	    
	    logger << setw(12) << instruction.to_string(SC_HEX_US, false);
	    logger << setw(13) << program_counter.to_string(SC_HEX_US, false);
	    logger << setw(11) << regfile_read_reg_1.to_string(SC_HEX_US, false);
	    logger << setw(20) << regfile_read_reg_2.to_string(SC_HEX_US, false);
	    logger << setw(20) << regfile_write_reg.to_string(SC_HEX_US, false);	    
	    logger << setw(25) << regfile_write_data.to_string(SC_HEX_US, false);	    
	    logger << setw(20) << regfile_read_data_1.to_string(SC_HEX_US, false);
	    logger << setw(21) << regfile_read_data_2.to_string(SC_HEX_US, false);
	    logger << setw(21) << mem_data_address.to_string(SC_HEX_US, false);
	    logger << setw(18) << mem_read_data.to_string(SC_HEX_US, false);
	    logger << setw(15) << mem_write_data.to_string(SC_HEX_US, false);
	    
	    logger << setw(9) << ALUSRC.read();
	    logger << setw(8) << MEMREAD.read();
	    logger << setw(9) << MEMWRITE.read();
	    logger << setw(10) << MEMTOREG.read();
	    logger << setw(10) << REGDST.read();
	    logger << setw(8) << REGWRITE.read();
	    logger << setw(10) << BRANCH_SEL.read();
	    logger << setw(12) << JUMP.read();	    
	    logger << setw(6) << alu_operation.to_string(SC_HEX_US, false);
	    logger << setw(15) << Z.read();
	    logger << setw(3) << V.read() << endl;
	    
	    // Exit condition
	    if(regfile_write_reg == sc_lv<5>("00010"))
	        sc_stop();
        }
    }

    SC_CTOR(checker_mips) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
	logger.open("sim.log");
    }

};

#endif