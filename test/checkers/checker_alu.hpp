#ifndef CHECKER_ALU_HPP
#define CHECKER_ALU_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_alu) {
    
    sc_in<sc_logic> A, B, CIN, LESS, A_INV, B_INV, RES, SET, COUT;
    sc_in<sc_logic> OPERATION[2];
    sc_in<bool> CLK;
    
    void check() {
      
        cout << setw(7) << "Time";  
	cout << setw(3) << "A";
	cout << setw(3) << "B";
	cout << setw(5) << "CIN";
	cout << setw(6) << "LESS";
	cout << setw(7) << "A_INV";
	cout << setw(7) << "B_INV";
	cout << setw(11) << "OPERATION";
	cout << setw(5) << "RES";
	cout << setw(5) << "SET";
	cout << setw(6) << "COUT" << endl;
        
	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();  
	    cout << setw(3) << A.read();
	    cout << setw(3) << B.read();
	    cout << setw(3) << CIN.read();
	    cout << setw(5) << LESS.read();
	    cout << setw(6) << A_INV.read();
	    cout << setw(7) << B_INV.read();
	    cout << setw(7) << OPERATION[1] << OPERATION[0];
	    cout << setw(10) << RES.read();
	    cout << setw(5) << SET.read();
	    cout << setw(5) << COUT.read() << endl;	    
        }
    }
    
    SC_CTOR(checker_alu) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
  
};

#endif
