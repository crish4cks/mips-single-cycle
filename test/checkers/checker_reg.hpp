#ifndef CHECKER_REG_HPP
#define CHECKER_REG_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_reg) {

    sc_in<sc_logic> RST, LOAD, D[32], Q[32];
    sc_in<bool> CLK;

    void check() {

        sc_lv<32> d, q;

        cout << setw(7) << "Time";  
	cout << setw(5) << "CLK";
	cout << setw(5) << "RST";
	cout << setw(6) << "LOAD";
        cout << setw(3) << "D";
        cout << setw(10) << "Q" << endl;

	while(true) {
	    wait(50, SC_NS); // wait one half of clock cycle
	    cout << setw(7) << sc_time_stamp();
            cout << setw(3) << CLK.read();
            cout << setw(5) << RST.read();
            cout << setw(5) << LOAD.read();
	    for(unsigned i = 0; i < 32; i++)
	        d[i] = D[i].read();
	    cout << setw(13) << d.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 32; i++)
	        q[i] = Q[i].read();
	    cout << setw(10) << q.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_reg) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
