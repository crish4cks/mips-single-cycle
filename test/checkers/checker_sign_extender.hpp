#ifndef CHECKER_SIGN_EXTENDER_HPP
#define CHECKER_SIGN_EXTENDER_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_sign_extender) {

    sc_in<sc_logic> INPUT[16], OUTPUT[32];
    sc_in<bool> CLK;

    void check() {

        sc_lv<16> input;
	sc_lv<32> output;

        cout << setw(7) << "Time";  
	cout << setw(5) << "CLK";
	cout << setw(7) << "INPUT";
	cout << setw(8) << "OUTPUT" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();
            cout << setw(3) << CLK.read();            
	    for(unsigned i = 0; i < 16; i++)
	        input[i] = INPUT[i].read();
	    cout << setw(8) << input.to_string(SC_HEX_US, false);
	    for(unsigned i = 0; i < 32; i++)
	        output[i] = OUTPUT[i].read();
	    cout << setw(11) << output.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_sign_extender) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }

};

#endif
