#ifndef CHECKER_INSTRUCTION_MEMORY_HPP
#define CHECKER_INSTRUCTION_MEMORY_HPP

#include <iostream>
#include <iomanip>
#include "systemc.h"

using namespace std;

SC_MODULE(checker_instruction_memory) {

    sc_in<sc_logic> ADDRESS[32], DATA[32];
    sc_in<bool> CLK;

    void check() {

        sc_lv<32> address, data;

        cout << setw(7) << "Time";  
	cout << setw(9) << "ADDRESS";
        cout << setw(7) << "DATA" << endl;

	while(true) {
	    wait(); // wait one clock cycle
	    cout << setw(7) << sc_time_stamp();            
	    for(unsigned i = 0; i < 32; i++) {
	        address[i] = ADDRESS[i].read();
		data[i] = DATA[i].read();
	    }
	    cout << setw(10) << address.to_string(SC_HEX_US, false);
	    cout << setw(10) << data.to_string(SC_HEX_US, false) << endl;
        }
    }

    SC_CTOR(checker_instruction_memory) {
        SC_THREAD(check);
	sensitive << CLK.pos();
	dont_initialize();
    }
    
};

#endif
