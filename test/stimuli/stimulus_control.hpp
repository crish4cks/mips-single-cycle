#ifndef STIMULUS_CONTROL_HPP
#define STIMULUS_CONTROL_HPP

#include "systemc.h"

SC_MODULE(stimulus_control) {
    sc_out<sc_logic> OPCODE[6];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<6> opcode;

        opcode = sc_lv<6>("000000");
	for(unsigned i = 0; i < 6; i++)
	    OPCODE[i].write(opcode[i]);
        wait();
	
	opcode = sc_lv<6>("100011");
	for(unsigned i = 0; i < 6; i++)
	    OPCODE[i].write(opcode[i]);
        wait();
	
	opcode = sc_lv<6>("101011");
	for(unsigned i = 0; i < 6; i++)
	    OPCODE[i].write(opcode[i]);
        wait();
	
	opcode = sc_lv<6>("000100");
	for(unsigned i = 0; i < 6; i++)
	    OPCODE[i].write(opcode[i]);
        wait();
	
	opcode = sc_lv<6>("111111");
	for(unsigned i = 0; i < 6; i++)
	    OPCODE[i].write(opcode[i]);
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_control) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
