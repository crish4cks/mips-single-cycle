#ifndef STIMULUS_SHIFTER_HPP
#define STIMULUS_SHIFTER_HPP

#include "systemc.h"

SC_MODULE(stimulus_shifter) {
    sc_out<sc_logic> INPUT[32], DIRECTION;
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<32> input;

        input = sc_lv<32>("00000000000000000000000000001010");
	for(unsigned i = 0; i < 32; i++)
	    INPUT[i].write(input[i]);
	DIRECTION.write(SC_LOGIC_0);
        wait();

        input = sc_lv<32>("00000000000000000000000000001010");
        for(unsigned i = 0; i < 32; i++) 
            INPUT[i].write(input[i]);
        DIRECTION.write(SC_LOGIC_1);
        wait();

        input = sc_lv<32>("10000000000000000000000000101011");
        for(unsigned i = 0; i < 32; i++) 
            INPUT[i].write(input[i]);
        DIRECTION.write(SC_LOGIC_0);
        wait();

        input = sc_lv<32>("00000000000000000001000000101011");
        for(unsigned i = 0; i < 32; i++) 
            INPUT[i].write(input[i]);
        DIRECTION.write(SC_LOGIC_1); 
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_shifter) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
