#ifndef STIMULUS_ALU_HPP
#define STIMULUS_ALU_HPP

#include "systemc.h"

SC_MODULE(stimulus_alu) {
    sc_out<sc_logic> A, B, CIN, LESS, A_INV, B_INV;
    sc_out<sc_logic> OPERATION[2];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        
        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_0);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_0);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_1);
	OPERATION[0].write(SC_LOGIC_0);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_1);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_1);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_1);
	OPERATION[0].write(SC_LOGIC_0);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_0);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_0);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_0);
	OPERATION[0].write(SC_LOGIC_1);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_0);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_0);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_0);
	OPERATION[0].write(SC_LOGIC_0);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_0);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_0);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_1);
	OPERATION[0].write(SC_LOGIC_1);
        wait();
	
	A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_1);
	A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_1);
	LESS.write(SC_LOGIC_0);
	OPERATION[1].write(SC_LOGIC_1);
	OPERATION[0].write(SC_LOGIC_0);
        wait();

        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_1);
        A_INV.write(SC_LOGIC_0);
        B_INV.write(SC_LOGIC_1);
        LESS.write(SC_LOGIC_1);
        OPERATION[1].write(SC_LOGIC_1);
        OPERATION[0].write(SC_LOGIC_1);
        wait();    
        
        sc_stop();
    }
  
    SC_CTOR(stimulus_alu) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif

