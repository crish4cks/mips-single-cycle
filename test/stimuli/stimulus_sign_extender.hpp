#ifndef STIMULUS_SIGN_EXTENDER_HPP
#define STIMULUS_SIGN_EXTENDER_HPP

#include "systemc.h"

SC_MODULE(stimulus_sign_extender) {
    sc_out<sc_logic> INPUT[16];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<16> input;

        input = sc_lv<16>("0000000000000010");
	for(unsigned i = 0; i < 16; i++)
	    INPUT[i].write(input[i]);	
        wait();
	
	input = sc_lv<16>("0000000000000101");
	for(unsigned i = 0; i < 16; i++)
	    INPUT[i].write(input[i]);	
        wait();
	
	input = sc_lv<16>("1000000000000001");
	for(unsigned i = 0; i < 16; i++)
	    INPUT[i].write(input[i]);	
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_sign_extender) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
