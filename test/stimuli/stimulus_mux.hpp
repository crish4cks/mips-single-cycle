#ifndef STIMULUS_MUX_HPP
#define STIMULUS_MUX_HPP

#include "systemc.h"

SC_MODULE(stimulus_mux) {
    sc_out<sc_logic> A[128];
    sc_out<sc_logic> SEL[2];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<128> a;
	sc_lv<2> sel;
	
        a = sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("01");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
      
        a = sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("00");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
	
	a = sc_lv<128>("00000000000000000000000000001010000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("11");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
	
	a = sc_lv<128>("00000000000000000000000000001000000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("11");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
	
	a = sc_lv<128>("00000000000000000000000000001001000000000000000000000000000010110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("11");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
	
	a = sc_lv<128>("00000000000000000000000000001000000000000000000000000000000001110000000000000000000000000000110000000000000000000000000000001111");
        sel = sc_lv<2>("10");
	for(unsigned i = 0; i < 128; i++) {
	    A[i].write(a[i]);
	}
	SEL[0].write(sel[0]);
	SEL[1].write(sel[1]);
        wait();
        
        sc_stop();
    }
  
    SC_CTOR(stimulus_mux) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif

