#ifndef STIMULUS_ALU32_HPP
#define STIMULUS_ALU32_HPP

#include "systemc.h"

SC_MODULE(stimulus_alu32) {
    sc_out<sc_logic> A[32], B[32], A_INV, B_NEG, OPERATION[2];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<32> a, b;
	sc_lv<2> operation;
	
        a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000000011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_0);
	operation = sc_lv<2>("10");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("01111111111111111111111111111111");
	b = sc_lv<32>("01111111111111111111111111111111");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_0);
	operation = sc_lv<2>("10");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
              
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000000011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_1);
	operation = sc_lv<2>("10");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000001011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_1);
	operation = sc_lv<2>("10");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000000011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_1);
	operation = sc_lv<2>("11");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000001011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_1);
	operation = sc_lv<2>("11");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00010000000001000000000000001010");
	b = sc_lv<32>("00010000100000000000000000001011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_0);
	operation = sc_lv<2>("00");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000001011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_0);
	B_NEG.write(SC_LOGIC_0);
	operation = sc_lv<2>("01");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
	a = sc_lv<32>("00000000000000000000000000001010");
	b = sc_lv<32>("00000000000000000000000000001011");
	for(unsigned i = 0; i < 32; i++) {
	    A[i].write(a[i]);
	    B[i].write(b[i]);
	}
	A_INV.write(SC_LOGIC_1);
	B_NEG.write(SC_LOGIC_1);
	operation = sc_lv<2>("00");
	OPERATION[0].write(operation[0]);
	OPERATION[1].write(operation[1]);
        wait();
	
        sc_stop();
    }
  
    SC_CTOR(stimulus_alu32) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif