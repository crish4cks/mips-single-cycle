#ifndef STIMULUS_REG_HPP
#define STIMULUS_REG_HPP

#include "systemc.h"

SC_MODULE(stimulus_reg) {
    sc_out<sc_logic> RST, LOAD, D[32];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<32> d;

        d = sc_lv<32>("00000000000000000000000000010000");
	for(unsigned i = 0; i < 32; i++)
	    D[i].write(d[i]);
	RST.write(SC_LOGIC_1);
        LOAD.write(SC_LOGIC_1);
        wait();

        d = sc_lv<32>("00000000000000000000000000011010");
        for(unsigned i = 0; i < 32; i++)
            D[i].write(d[i]);
        RST.write(SC_LOGIC_1);
        LOAD.write(SC_LOGIC_0);
        wait();

        d = sc_lv<32>("00000000000000000000000010101010");
        for(unsigned i = 0; i < 32; i++)
            D[i].write(d[i]);
        RST.write(SC_LOGIC_1);
        LOAD.write(SC_LOGIC_1);
        wait();

        d = sc_lv<32>("00000000000000000000000010101011");
        for(unsigned i = 0; i < 32; i++)
            D[i].write(d[i]);
        RST.write(SC_LOGIC_1);
        LOAD.write(SC_LOGIC_1);
        wait();

        d = sc_lv<32>("00000000000000000000000010101010");
        for(unsigned i = 0; i < 32; i++)
            D[i].write(d[i]);
        RST.write(SC_LOGIC_0);
        LOAD.write(SC_LOGIC_0);
        wait();

        d = sc_lv<32>("00000000000000000000000010101010");
        for(unsigned i = 0; i < 32; i++)
            D[i].write(d[i]);
        RST.write(SC_LOGIC_0);
        LOAD.write(SC_LOGIC_1);
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_reg) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
