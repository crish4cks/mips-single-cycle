#ifndef STIMULUS_DATA_MEMORY_HPP
#define STIMULUS_DATA_MEMORY_HPP

#include "systemc.h"

SC_MODULE(stimulus_data_memory) {
    sc_out<sc_logic> RST, ADDRESS[32], WRITE_DATA[32], MEMREAD, MEMWRITE;
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<32> address, write_data;

        address = sc_lv<32>("00000000000000000000000000000000");
	write_data = sc_lv<32>("00000000000000000000000010111010");
	for(unsigned i = 0; i < 32; i++) {
	    ADDRESS[i].write(address[i]);
	    WRITE_DATA[i].write(write_data[i]);	    
	}
	RST.write(SC_LOGIC_1);
	MEMREAD.write(SC_LOGIC_0);
        MEMWRITE.write(SC_LOGIC_1);
        wait();
	
	address = sc_lv<32>("00000000000000000000000000000000");
	write_data = sc_lv<32>("00000000000000000000000110111010");
	for(unsigned i = 0; i < 32; i++) {
	    ADDRESS[i].write(address[i]);
	    WRITE_DATA[i].write(write_data[i]);	    
	}
	RST.write(SC_LOGIC_1);
	MEMREAD.write(SC_LOGIC_1);
        MEMWRITE.write(SC_LOGIC_0);
        wait();
	
	address = sc_lv<32>("00000000000000000000000000000000");
	write_data = sc_lv<32>("00000000000000000000000110111010");
	for(unsigned i = 0; i < 32; i++) {
	    ADDRESS[i].write(address[i]);
	    WRITE_DATA[i].write(write_data[i]);	    
	}
	RST.write(SC_LOGIC_0);
	MEMREAD.write(SC_LOGIC_1);
        MEMWRITE.write(SC_LOGIC_0);
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_data_memory) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
