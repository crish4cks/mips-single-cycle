#ifndef STIMULUS_NOT_GATE_HPP
#define STIMULUS_NOT_GATE_HPP

#include "systemc.h"

SC_MODULE(stimulus_not_gate) {
    sc_out<sc_logic> A;
    sc_in<bool> CLK;

    void StimulusGenerator() {
        
        A.write(SC_LOGIC_0);
        wait();
      
        A.write(SC_LOGIC_1);
        wait();
        
        sc_stop();
    }
  
    SC_CTOR(stimulus_not_gate) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif