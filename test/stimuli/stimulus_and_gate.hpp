#ifndef STIMULUS_AND_GATE_HPP
#define STIMULUS_AND_GATE_HPP

#include "systemc.h"

SC_MODULE(stimulus_and_gate) {
    sc_out<sc_logic> A[3];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        
        A[2].write(SC_LOGIC_0);
	A[1].write(SC_LOGIC_0);
	A[0].write(SC_LOGIC_0);
        wait();
        
	A[2].write(SC_LOGIC_0);
	A[1].write(SC_LOGIC_0);
	A[0].write(SC_LOGIC_1);
        wait();
	
	A[2].write(SC_LOGIC_0);
	A[1].write(SC_LOGIC_1);
	A[0].write(SC_LOGIC_0);
        wait();
	
	A[2].write(SC_LOGIC_0);
	A[1].write(SC_LOGIC_1);
	A[0].write(SC_LOGIC_1);
        wait();
	
	A[2].write(SC_LOGIC_1);
	A[1].write(SC_LOGIC_0);
	A[0].write(SC_LOGIC_0);
        wait();
	
	A[2].write(SC_LOGIC_1);
	A[1].write(SC_LOGIC_0);
	A[0].write(SC_LOGIC_1);
        wait();
	
	A[2].write(SC_LOGIC_1);
	A[1].write(SC_LOGIC_1);
	A[0].write(SC_LOGIC_0);
        wait();
	
	A[2].write(SC_LOGIC_1);
	A[1].write(SC_LOGIC_1);
	A[0].write(SC_LOGIC_1);
        wait();
        
        sc_stop();
    }
  
    SC_CTOR(stimulus_and_gate) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif