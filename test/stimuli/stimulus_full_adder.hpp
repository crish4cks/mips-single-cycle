#ifndef STIMULUS_FULL_ADDER_HPP
#define STIMULUS_FULL_ADDER_HPP

#include "systemc.h"

SC_MODULE(stimulus_full_adder) {
    sc_out<sc_logic> A, B, CIN;
    sc_in<bool> CLK;

    void StimulusGenerator() {
        
        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_0);
        wait();
      
        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_1);
        wait();
    
        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_0);
        wait();

        A.write(SC_LOGIC_0);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_1);
        wait();

        A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_0);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_0);
        CIN.write(SC_LOGIC_1);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_0);
        wait();
	
	A.write(SC_LOGIC_1);
        B.write(SC_LOGIC_1);
        CIN.write(SC_LOGIC_1);
        wait();
        
        sc_stop();
    }
  
    SC_CTOR(stimulus_full_adder) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif

