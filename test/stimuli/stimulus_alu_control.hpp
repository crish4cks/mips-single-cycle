#ifndef STIMULUS_ALU_CONTROL_HPP
#define STIMULUS_ALU_CONTROL_HPP

#include "systemc.h"

SC_MODULE(stimulus_alu_control) {
    sc_out<sc_logic> ALUOP[2], FUNCTION_CODE[6];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<6> function_code;
     	
	ALUOP[1].write(SC_LOGIC_0);
	ALUOP[0].write(SC_LOGIC_1);
        function_code = sc_lv<6>("110000");
	for(unsigned i = 0; i < 6; i++)
	    FUNCTION_CODE[i].write(function_code[i]);
        wait();
	
	ALUOP[1].write(SC_LOGIC_1);
	ALUOP[0].write(SC_LOGIC_0);
        function_code = sc_lv<6>("100010");
	for(unsigned i = 0; i < 6; i++)
	    FUNCTION_CODE[i].write(function_code[i]);
        wait();
	
	ALUOP[1].write(SC_LOGIC_1);
	ALUOP[0].write(SC_LOGIC_0);
        function_code = sc_lv<6>("101010");
	for(unsigned i = 0; i < 6; i++)
	    FUNCTION_CODE[i].write(function_code[i]);
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_alu_control) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
