#ifndef STIMULUS_MIPS_HPP
#define STIMULUS_MIPS_HPP

#include "systemc.h"
#include <fstream>
#include <string>

SC_MODULE(stimulus_mips) {
    sc_out<sc_logic> RST;
    sc_in<bool> CLK;

    void StimulusGenerator() {
	while(true) {
	    RST.write(SC_LOGIC_1);
	    wait();  
	}
    }
  
    SC_CTOR(stimulus_mips) {
	RST.initialize(SC_LOGIC_0);        
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif