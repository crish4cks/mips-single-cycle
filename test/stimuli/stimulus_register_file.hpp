#ifndef STIMULUS_REGISTER_FILE_HPP
#define STIMULUS_REGISTER_FILE_HPP

#include "systemc.h"

SC_MODULE(stimulus_register_file) {
    sc_out<sc_logic> RST, REGWRITE, READ_REGISTER_1[5], READ_REGISTER_2[5], WRITE_REGISTER[5], WRITE_DATA[32];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<5> read_register_1, read_register_2, write_register;
        sc_lv<32> write_data;
	
	RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_0);
        read_register_1 = sc_lv<5>("00001");
	read_register_2 = sc_lv<5>("00010");
	write_register = sc_lv<5>("00101");
	write_data = sc_lv<32>("00000000000000000000000000001010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();

        RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_1);
        read_register_1 = sc_lv<5>("00001");
	read_register_2 = sc_lv<5>("00010");
	write_register = sc_lv<5>("00101");
	write_data = sc_lv<32>("00000000000000000000000000011010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();

	RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_1);
        read_register_1 = sc_lv<5>("00001");
	read_register_2 = sc_lv<5>("00010");
	write_register = sc_lv<5>("00010");
	write_data = sc_lv<32>("00000000000000000000000000101010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();
	
	RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_0);
        read_register_1 = sc_lv<5>("00010");
	read_register_2 = sc_lv<5>("00101");
	write_register = sc_lv<5>("00101");
	write_data = sc_lv<32>("00000000000000000000000000011010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();
	
	RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_0);
        read_register_1 = sc_lv<5>("00010");
	read_register_2 = sc_lv<5>("00101");
	write_register = sc_lv<5>("00101");
	write_data = sc_lv<32>("00000000000000000000000000011010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();
	
	RST.write(SC_LOGIC_1);
        REGWRITE.write(SC_LOGIC_0);
        read_register_1 = sc_lv<5>("01001");
	read_register_2 = sc_lv<5>("00110");
	write_register = sc_lv<5>("00111");
	write_data = sc_lv<32>("00000000000000000000000000011010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();
	
	RST.write(SC_LOGIC_0);
        REGWRITE.write(SC_LOGIC_0);
        read_register_1 = sc_lv<5>("00010");
	read_register_2 = sc_lv<5>("00101");
	write_register = sc_lv<5>("00101");
	write_data = sc_lv<32>("00000000000000000000000000011010");
	for(unsigned i = 0; i < 5; i++) {
	    READ_REGISTER_1[i].write(read_register_1[i]);
	    READ_REGISTER_2[i].write(read_register_2[i]);
	    WRITE_REGISTER[i].write(write_register[i]);
	}
	for(unsigned i = 0; i < 32; i++)	   
	    WRITE_DATA[i].write(write_data[i]);
        wait();
	
        sc_stop();
    }
  
    SC_CTOR(stimulus_register_file) {
        RST.initialize(SC_LOGIC_0);
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
