#ifndef STIMULUS_INSTRUCTION_MEMORY_HPP
#define STIMULUS_INSTRUCTION_MEMORY_HPP

#include "systemc.h"

SC_MODULE(stimulus_instruction_memory) {
    sc_out<sc_logic> ADDRESS[32];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<32> address;

        address = sc_lv<32>("00000000000000000000000000000000");	
	for(unsigned i = 0; i < 32; i++)
	    ADDRESS[i].write(address[i]);
        wait();
	
	address = sc_lv<32>("00000000000000000000000000000100");	
	for(unsigned i = 0; i < 32; i++)
	    ADDRESS[i].write(address[i]);
        wait();
	
	address = sc_lv<32>("00000000000000000000000000001000");	
	for(unsigned i = 0; i < 32; i++)
	    ADDRESS[i].write(address[i]);
        wait();
	
	address = sc_lv<32>("00000000000000000000000000001100");	
	for(unsigned i = 0; i < 32; i++)
	    ADDRESS[i].write(address[i]);
        wait();

        sc_stop();
    }
  
    SC_CTOR(stimulus_instruction_memory) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
