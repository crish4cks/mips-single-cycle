#ifndef STIMULUS_DECODER_HPP
#define STIMULUS_DECODER_HPP

#include "systemc.h"

SC_MODULE(stimulus_decoder) {
    sc_out<sc_logic> X[5];
    sc_in<bool> CLK;

    void StimulusGenerator() {
        sc_lv<5> x;

        x = sc_lv<5>("00011");
	for(unsigned i = 0; i < 5; i++)
	    X[i].write(x[i]);
        wait();        
     
        x = sc_lv<5>("01010");
	for(unsigned i = 0; i < 5; i++)
	    X[i].write(x[i]);
        wait();
	
	x = sc_lv<5>("11111");
	for(unsigned i = 0; i < 5; i++)
	    X[i].write(x[i]);
        wait();
	
        sc_stop();
    }
  
    SC_CTOR(stimulus_decoder) {
        SC_THREAD(StimulusGenerator);
        sensitive << CLK.pos();
	dont_initialize();
    }
};

#endif
